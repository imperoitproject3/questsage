﻿using QuestSageProject.DAL;
using System;
using System.Linq;
using System.Web.UI;

namespace QuestSageProject.Admin
{
    public partial class editinquiry : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                {
                    int InquiryId = Convert.ToInt32(Request.QueryString["id"].ToString());
                    BindEditEnquiry(InquiryId);
                }
                else
                    Response.Redirect("inquiries.aspx");
            }
        }

        private void BindEditEnquiry(int InquiryId)
        {
            QuestsageEntities _ctx = new QuestsageEntities();
            Inquiry data = _ctx.Inquiry.FirstOrDefault(x => x.Id == InquiryId);
            if (data == null)
            {
                Response.Redirect("inquiries.aspx");
            }
            else
            {
                txtName.Text = data.Name;
                txtCountry.Text = data.Country;
                txtEmail.Text = data.Email;
                txtnumber.Text = data.ContactNumber;
                txtSkype.Text = data.Skype;
                chkNDARequired.Checked = data.NDARequired;
                txtProjectDetail.Text = data.ProjectDetails;
                txtRemark.Text = data.Remarks;

                if (!string.IsNullOrEmpty(data.DocumentFileName))
                {
                    lnkAttachment.NavigateUrl = Page.ResolveUrl("~/Files/InquiryDocs/" + data.DocumentFileName);
                    lnkAttachment.Text = "Click Here";
                    divAttachment.Style.Add("display", "block;");
                }
                else
                    divAttachment.Style.Add("display", "none;");
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            int InquiryId = Convert.ToInt32(Request.QueryString["id"].ToString());
            QuestsageEntities _ctx = new QuestsageEntities();

            Inquiry data = _ctx.Inquiry.FirstOrDefault(x => x.Id == InquiryId);
            if (data == null)
            {
                Response.Redirect("inquiries.aspx");
            }
            else
            {
                data.Remarks = txtRemark.Text;
                _ctx.SaveChanges();
                Response.Redirect("inquiries.aspx");
            }
        }
    }
}