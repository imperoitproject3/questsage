﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/QuestSageAdmin.Master" AutoEventWireup="true" CodeBehind="addcategory.aspx.cs" Inherits="QuestSageProject.Admin.addcategory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="<%= Page.ResolveUrl("~/Content/Plugins/Summernote/summernote.css") %>" rel="stylesheet" />

    <script src="<%= Page.ResolveUrl("~/Content/Plugins/Summernote/summernote.min.js") %>"></script>
    <script type="text/javascript">
        $(function () {
            $('#liCategories').addClass('active');            
        });

        function fnDuplicateCategory() {
            alert('Another category already exists with same name!');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">    
    <section id="validation">
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Add New Category</h4>
                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    </div>
                    <div class="card-body collapse in">
                        <div class="card-block">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-md-2">Category <span class="red">*</span> <span class="icon-question-circle" data-toggle="tooltip" data-offerment="top" title="" data-original-title="Blog Category"></span></label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txtName" runat="server" CssClass="form-control" MaxLength="200"></asp:TextBox>
                                            <asp:RequiredFieldValidator Style="color: red;" runat="server" ControlToValidate="txtName" ErrorMessage="Please enter category name"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>                                    

                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn btn-md btn-primary" />
                                            <a href="<%= Page.ResolveUrl("categories.aspx") %>" class="btn btn-md btn-grey">Cancel
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>