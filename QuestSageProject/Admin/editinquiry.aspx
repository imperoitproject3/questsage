﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/QuestSageAdmin.Master" AutoEventWireup="true" CodeBehind="editinquiry.aspx.cs" Inherits="QuestSageProject.Admin.editinquiry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $('#liInquiries').addClass('active');
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="validation">
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Edit Enquiry</h4>
                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    </div>
                    <div class="card-body collapse in">
                        <div class="card-block">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-md-2">Name <span class="red">*</span> <span class="icon-question-circle" data-toggle="tooltip" data-offerment="top" title="" data-original-title="User Name"></span></label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txtName" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-2">Country <span class="red">*</span> <span class="icon-question-circle" data-toggle="tooltip" data-offerment="top" title="" data-original-title="Country"></span></label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txtCountry" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-2">Email <span class="red">*</span> <span class="icon-question-circle" data-toggle="tooltip" data-offerment="top" title="" data-original-title="Email Address"></span></label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-2">Number <span class="red">*</span> <span class="icon-question-circle" data-toggle="tooltip" data-offerment="top" title="" data-original-title="Contact Number"></span></label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txtnumber" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-2">Skype <span class="icon-question-circle" data-toggle="tooltip" data-offerment="top" title="" data-original-title="Contact Number"></span></label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txtSkype" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-2">NDA Required <span class="red">*</span> <span class="icon-question-circle" data-toggle="tooltip" data-offerment="top" title="" data-original-title="NDA Required"></span></label>
                                        <div class="col-md-4">
                                            <asp:CheckBox ID="chkNDARequired" runat="server" Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group row" id="divAttachment" runat="server">
                                        <label class="col-md-2">Document <span class="red">*</span> <span class="icon-question-circle" data-toggle="tooltip" data-offerment="top" title="" data-original-title="Document"></span></label>
                                        <div class="col-md-4">
                                            <asp:HyperLink ID="lnkAttachment" runat="server"></asp:HyperLink>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-2">Project Detail <span class="red">*</span> <span class="icon-question-circle" data-toggle="tooltip" data-offerment="top" title="" data-original-title="Project Detail"></span></label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txtProjectDetail" runat="server" CssClass="form-control" Enabled="false" TextMode="MultiLine"></asp:TextBox>
                                        </div>
                                    </div>                                   

                                    <div class="form-group row">
                                        <label class="col-md-2">Remark <span class="red">*</span> <span class="icon-question-circle" data-toggle="tooltip" data-offerment="top" title="" data-original-title="Remark"></span></label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txtRemark" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn btn-md btn-primary" />
                                            <a href="<%= Page.ResolveUrl("inquiries.aspx") %>" class="btn btn-md btn-grey">Cancel
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>