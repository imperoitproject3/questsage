﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/QuestSageAdmin.Master" AutoEventWireup="true" CodeBehind="inquiries.aspx.cs" Inherits="QuestSageProject.Admin.inquiries" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(function () {
            $('#liInquiries').addClass('active');
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section id="basic">
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">All Inquiries</h4>
                        <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    </div>
                    <div class="card-body collapse in">
                        <div class="card-block card-dashboard">
                            <div id="basicScenario" class="jsgrid" style="position: relative; height: auto; width: 100%;">
                                <div class="jsgrid-grid-body">
                                    <table class="Datatable table table-hover table-striped" style="margin-top: 0px;">
                                        <thead style="display: table-header-group;">
                                            <tr>
                                                <th style="" data-field="0" tabindex="0">
                                                    <div class="th-inner">Name</div>
                                                    <div class="fht-cell"></div>
                                                </th>
                                                <th style="" data-field="0" tabindex="0">
                                                    <div class="th-inner">Country</div>
                                                    <div class="fht-cell"></div>
                                                </th>
                                                <th style="" data-field="0" tabindex="0">
                                                    <div class="th-inner">Email</div>
                                                    <div class="fht-cell"></div>
                                                </th>
                                                <th style="" data-field="0" tabindex="0">
                                                    <div class="th-inner">Budget</div>
                                                    <div class="fht-cell"></div>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Repeater runat="server" ID="rptInquiries" OnItemCommand="rptInquiries_ItemCommand">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Eval("Name")%></td>
                                                        <td><%# Eval("Country")%></td>
                                                        <td><%# Eval("Email")%></td>
                                                        <td><%# Eval("Budget")%></td>
                                                        <td>
                                                            <a class="btn btn-sm btn-info" href="editinquiry.aspx?id=<%# Eval("Id")%>">Detail
                                                            </a>
                                                            <asp:LinkButton runat="server" class="btn btn-sm btn-danger m_top_5" CommandArgument='<%# Eval("Id")%>' CommandName="delete"
                                                                OnClientClick="return ConfirmOnDelete();">Delete
                                                            </asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>