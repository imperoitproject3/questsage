﻿using QuestSageProject.DAL;
using System;
using System.Linq;

namespace QuestSageProject.Admin
{
    public partial class addcategory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            QuestsageEntities _ctx = new QuestsageEntities();

            Category data = _ctx.Category.FirstOrDefault(x => x.Name.ToLower() == txtName.Text.ToString().ToLower());
            if (data != null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "DuplicateCategory", "fnDuplicateCategory()", true);
            }
            else
            {
                Category objCategory = new Category
                {
                    Name = txtName.Text
                };
                _ctx.Category.Add(objCategory);
                _ctx.SaveChanges();
                Response.Redirect("categories.aspx");
            }            
        }
    }
}