﻿using QuestSageProject.DAL;
using QuestSageProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuestSageProject.Admin
{
    public partial class inquiries : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindInquiries();
            }
        }

        private void BindInquiries()
        {
            QuestsageEntities _ctx = new QuestsageEntities();

            List<InquiryModel> lstInquiries = (from i in _ctx.Inquiry
                                               select new InquiryModel
                                               {
                                                   Id = i.Id,
                                                   Name = i.Name,
                                                   Budget = i.Budget,
                                                   Country = i.Country,
                                                   Email = i.Email,
                                                   ContactNumber = i.ContactNumber
                                               }).ToList();

            if (lstInquiries != null && lstInquiries.Any())
                rptInquiries.DataSource = lstInquiries;
            else
                rptInquiries.DataSource = null;
            rptInquiries.DataBind();
        }

        protected void rptInquiries_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "delete" && e.CommandArgument != null)
            {
                int InquiryId = Convert.ToInt32(e.CommandArgument);
                if (InquiryId > 0)
                {
                    QuestsageEntities _ctx = new QuestsageEntities();
                    Inquiry objDelete = _ctx.Inquiry.FirstOrDefault(x => x.Id == InquiryId);
                    if (objDelete != null)
                    {
                        _ctx.Inquiry.Remove(objDelete);
                        _ctx.SaveChanges();
                        BindInquiries();
                    }
                }
            }
        }
    }
}