﻿using QuestSageProject.DAL;
using QuestSageProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuestSageProject.Admin
{
    public partial class blogs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindGrid();
            }
        }

        private void BindGrid()
        {
            QuestsageEntities _ctx = new QuestsageEntities();
            List<BlogMasterModel> blogs = (from b in _ctx.BlogMaster
                                           select new BlogMasterModel
                                           {
                                               BlogId = b.BlogId,
                                               CategoryId = b.CategoryId,
                                               CategoryName = b.Category.Name,
                                               Title = b.Title,
                                               Tag = b.Tag,
                                               Author = b.Author,
                                               PublishDate = b.PublishDate
                                           }).ToList();

            GV.DataSource = blogs;
            GV.DataBind();
        }

        protected void GV_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "delete" && e.CommandArgument != null)
            {
                int BlogId = Convert.ToInt32(e.CommandArgument);
                if (BlogId > 0)
                {
                    QuestsageEntities _ctx = new QuestsageEntities();
                    BlogMaster objDelete = _ctx.BlogMaster.FirstOrDefault(x => x.BlogId == BlogId);
                    if (objDelete != null)
                    {
                        _ctx.BlogMaster.Remove(objDelete);
                        _ctx.SaveChanges();
                        BindGrid();
                    }
                }
            }
        }
    }
}