﻿using QuestSageProject.DAL;
using QuestSageProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuestSageProject.Admin
{
    public partial class categories : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindGrid();
            }
        }

        private void BindGrid()
        {
            QuestsageEntities _ctx = new QuestsageEntities();
            List<CategoryModel> blogs = (from c in _ctx.Category
                                         select new CategoryModel
                                         {
                                             Id = c.Id,
                                             Name = c.Name
                                         }).ToList();

            GV.DataSource = blogs;
            GV.DataBind();
        }

        protected void GV_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "delete" && e.CommandArgument != null)
            {
                int Category = Convert.ToInt32(e.CommandArgument);
                if (Category > 0)
                {
                    QuestsageEntities _ctx = new QuestsageEntities();
                    Category objDelete = _ctx.Category.FirstOrDefault(x => x.Id == Category);
                    if (objDelete != null)
                    {
                        _ctx.Category.Remove(objDelete);
                        _ctx.SaveChanges();
                        BindGrid();
                    }
                }
            }
        }
    }
}