﻿using QuestSageProject.DAL;
using QuestSageProject.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace QuestSageProject.Admin
{
    public partial class addblog : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {            
            if (!Page.IsPostBack)
            {
                BindCategoryDropdown();
            }
        }

        private void BindCategoryDropdown()
        {
            QuestsageEntities _ctx = new QuestsageEntities();
            List<CategoryModel> categories = (from c in _ctx.Category
                                              select new CategoryModel
                                              {
                                                  Id = c.Id,
                                                  Name = c.Name
                                              }).ToList();

            ddlCategory.DataSource = categories;
            ddlCategory.DataBind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            QuestsageEntities _ctx = new QuestsageEntities();

            BlogMaster data = _ctx.BlogMaster.FirstOrDefault(x => x.Title.ToLower() == txtTitle.Text.ToString().ToLower());
            if (data != null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "DuplicateBlog", "fnDuplicateBlog()", true);
            }
            else
            {
                if (fileImage != null && fileImage.HasFile)
                {
                    BlogMaster objBlog = new BlogMaster
                    {
                        CategoryId = Convert.ToInt32(ddlCategory.SelectedValue),
                        Title = txtTitle.Text.ToString(),
                        Author = txtAuthor.Text,
                        PublishDate = DateTime.Now,
                        ImageUrl = "A",
                        BlogBody = txtDescription.Text,
                        Tag = txtTag.Text
                    };

                    string ogFileName = fileImage.FileName.Trim('\"');
                    string shortGuid = Guid.NewGuid().ToString();
                    var thisFileName = shortGuid + Path.GetExtension(ogFileName);

                    string BlogImagesPath = Server.MapPath("~/Files/BlogImages/");
                    fileImage.SaveAs(Path.Combine(BlogImagesPath, thisFileName));
                    objBlog.ImageUrl = thisFileName;
                    _ctx.BlogMaster.Add(objBlog);
                    _ctx.SaveChanges();
                }
                Response.Redirect("blogs.aspx");
            }
        }
    }
}