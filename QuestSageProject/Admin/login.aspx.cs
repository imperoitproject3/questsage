﻿using QuestSageProject.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuestSageProject.Admin
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            QuestsageEntities _ctx = new QuestsageEntities();
            DAL.Login objLogin = _ctx.Login.FirstOrDefault(x => x.UserName == emailId.Text && x.Password == password.Text);
            if (objLogin != null)
            {
                Session["AdminID"] = objLogin.AdminID;
                Response.Redirect("blogs.aspx");
            }
            else
            {
                lblError.Text = "Invalid username or password";
            }
        }
    }
}