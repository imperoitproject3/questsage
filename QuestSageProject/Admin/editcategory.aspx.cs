﻿using QuestSageProject.DAL;
using System;
using System.Linq;
using System.Web.UI;

namespace QuestSageProject.Admin
{
    public partial class editcategory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["CategoryId"]))
                {
                    BindCategory(Convert.ToInt32(Request.QueryString["CategoryId"]));
                }
            }
        }

        private void BindCategory(int CategoyId)
        {
            QuestsageEntities _ctx = new QuestsageEntities();
            Category data = _ctx.Category.FirstOrDefault(x => x.Id == CategoyId);
            if (data == null)
                Response.Redirect("categories.aspx");
            else
            {
                hdnCategoryId.Value = data.Id.ToString();
                txtName.Text = data.Name;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            QuestsageEntities _ctx = new QuestsageEntities();

            int CategoryId = Convert.ToInt32(hdnCategoryId.Value);

            Category data = _ctx.Category.FirstOrDefault(x => x.Name.ToLower() == txtName.Text.ToString().ToLower() && x.Id != CategoryId);
            if (data != null)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "DuplicateCategory", "fnDuplicateCategory()", true);
            }
            else
            {
                data = _ctx.Category.FirstOrDefault(x => x.Id == CategoryId);
                data.Name = txtName.Text;
                _ctx.SaveChanges();
                Response.Redirect("categories.aspx");
            }            
        }
    }
}