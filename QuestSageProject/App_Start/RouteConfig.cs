using Microsoft.AspNet.FriendlyUrls;
using System.Web.Routing;

namespace QuestSageProject
{
    public static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            var settings = new FriendlyUrlSettings();
            settings.AutoRedirectMode = RedirectMode.Off;

            routes.MapPageRoute("Blog",
                "blogdetail/{title}/{id}",
                "~/blogdetail.aspx");

            routes.EnableFriendlyUrls(settings);
        }
    }
}