﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.UI;

namespace QuestSageProject
{
    public class BundleConfig
    {
        // For more information on Bundling, visit https://go.microsoft.com/fwlink/?LinkID=303951
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/bundles/Styles").Include(
               "~/css/fontawesome-all.min.css",
               "~/css/bootstrap.min.css",
               "~/plugins/swiper/swiper.min.css",
               "~/plugins/magnific-popup/magnific-popup.min.css",
               "~/css/animate.min.css",
               "~/css/style.css",
               "~/css/responsive.css",
               "~/css/custom.css"
                ));


            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-3.3.1.js",
                "~/js/bootstrap.bundle.min.js",
                "~/plugins/sticky/sticky-header.min.js",
                "~/plugins/swiper/swiper.min.js",
                "~/plugins/magnific-popup/jquery.magnific-popup.min.js",
                "~/plugins/parsley/parsley.min.js",
                "~/plugins/retinajs/retina.min.js",
                "~/plugins/waypoints/jquery.waypoints.min.js",
                "~/js/menu.js",
                "~/js/scripts.js"
            ));

            BundleTable.EnableOptimizations = true;
        }
    }
}