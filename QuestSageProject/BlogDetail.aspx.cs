﻿using QuestSageProject.DAL;
using QuestSageProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuestSageProject
{
    public partial class BlogDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.RequestContext.RouteData.Values.ContainsKey("title") && Request.RequestContext.RouteData.Values.ContainsKey("id"))
            {
                string title = Request.RequestContext.RouteData.Values["title"].ToString();
                int BlogId = Convert.ToInt32(Request.RequestContext.RouteData.Values["id"]);

                BindBlogDetail(BlogId);
                BindRecentBlogs(BlogId);
                BindCategories();
            }
            else
                Response.Redirect("~/blog.aspx");
        }

        private void BindRecentBlogs(int BlogId)
        {
            QuestsageEntities _ctx = new QuestsageEntities();

            List<BlogMasterModel> objBlog = (from b in _ctx.BlogMaster
                                             where b.BlogId != BlogId
                                             select new BlogMasterModel
                                             {
                                                 BlogId = b.BlogId,
                                                 Title = b.Title,
                                                 PublishDate = b.PublishDate
                                             }).OrderByDescending(x => x.PublishDate).Take(5).ToList();

            if (objBlog != null && objBlog.Any())
            {
                rptRecentPost.DataSource = objBlog;
            }
            else
            {
                divRecentPost.Visible = false;
                rptRecentPost.DataSource = null;
            }
            rptRecentPost.DataBind();
        }

        private void BindBlogDetail(int BlogId)
        {
            QuestsageEntities _ctx = new QuestsageEntities();

            BlogMasterModel model = (from b in _ctx.BlogMaster
                                     select new BlogMasterModel
                                     {
                                         CategoryId = b.CategoryId,
                                         BlogId = b.BlogId,
                                         Title = b.Title,
                                         Author = b.Author,
                                         CategoryName = b.Category.Name,
                                         Tag = b.Tag,
                                         PublishDate = b.PublishDate,
                                         BlogBody = b.BlogBody,
                                         ImageUrl = b.ImageUrl
                                     }).FirstOrDefault();

            if (model == null)
                Response.Redirect("~/Blog.aspx");

            ltlCategoryName.Text = model.CategoryName;
            ltlPostedOn.Text = model.PublishDate.ToString("MMMM dd, yyyy");
            ltlBlogTitle.Text = model.Title;
            ltlPostedBy.Text = model.Author;
            ltlTagName.Text = model.Tag;
            CategoryLink.HRef = Page.ResolveUrl("Blog.aspx?CategoryId=" + model.CategoryId);
            blogImage.ImageUrl = Page.ResolveUrl("~/Files/BlogImages/" + model.ImageUrl);
            divBlogBody.InnerHtml = model.BlogBody;
        }

        public static string ToTitle(string value)
        {
            return value.Replace(" ", "-").ToLower();
        }

        private void BindCategories()
        {
            QuestsageEntities _ctx = new QuestsageEntities();

            List<CategoryModel> lstCategories = (from c in _ctx.Category
                                                 select new CategoryModel
                                                 {
                                                     Id = c.Id,
                                                     Name = c.Name,
                                                     BlogCount = c.BlogMaster.Count()
                                                 }).Where(x => x.BlogCount > 0).ToList();

            rptCategories.DataSource = lstCategories;
            rptCategories.DataBind();
        }
    }
}