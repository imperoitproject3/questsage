﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewMaster.master" AutoEventWireup="true" CodeBehind="FAQs.aspx.cs" Inherits="QuestSageProject.FAQs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="title-bg-darkfaqs">
        <div class="container">
            <div class="page-title text-white text-center">
                <h1 data-animate="fadeInUp" data-delay="1.2">FAQs </h1>
             <%--   <h2 data-animate="fadeInUp" data-delay="1.4">All your questions answered :)</h2>--%>
            </div>
        </div>
    </section>
    <section class="pt-70">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 ">
                    <div class="single-post-page blog-left">
                        <div class="blog-details">
                            <div class="post-detail-content">
                                <p data-animate="fadeInUp" data-delay=".2">
                                    <h3>Work Process Related Queries</h3>
                                    <br />
                                    <br />
                                    <b>Q: What are the services provided by QuestSage IT Services?<br />
                                    </b>A: QuestSage IT Services provides comprehensive mobile app development solutions.
                                    <br />
                                    <br />
                                    <b>Q: How can we reach QuestSage IT Services?
                                    <br />
                                    </b>A: You can contact us through any convenient mode such as a Phone call, Email, Skype, etc. All details are provided in our contact us section.
                                    <br />
                                    <br />
                                    <b>Q: What types of teams does QuestSage IT Services have and how skilled are they?
                                    <br />
                                    </b>A: We have a bunch of experienced and dedicated team of professional developers, designers, marketers, and BDEs, who have proven skills in the field of app development and web development.
                                    <br />
                                    <br />
                                    <b>Q: Why should I choose Quest Sage Solution?
                                    <br />
                                    </b>A: You can choose our services if you want timely, competent, and professional website and application development solutions. Our teams with diverse skills provide impeccable work and have a trail of satisfied clients who can be referred to.
                                    <br />
                                    <br />
                                    <b>Q: Can you assure me that my mobile app idea won’t be misused?
                                    <br />
                                    </b>A: While signing the non-disclosure agreement in the initial steps, QuestSage IT Services and the client protect the original source code, app code, and all other information related to the app and this is handed over to the client once the project is concluded.
                                    <br />
                                    <br />
                                    <b>Q: How long does it take to develop an app?
                                    <br />
                                    </b>A: The app development time takes somewhere between 1-3 months. However, this varies based on the client’s requirements and the type of app they are looking for.
                                    <br />
                                    <br />
                                    <b>Q: How can I monitor the development of my app project?
                                    <br />
                                    </b>A: Our team updates you on the progress of your project on a regular basis.
Pricing Related Queries
                                    <br />
                                    <br />
                                    <b>Q: How much will it cost to develop an app?
                                    <br />
                                    </b>A: Based on the specifications of the app, the price may vary. Our team can calculate and let you know the price once you tell them about your requirements.
                                    <br />
                                    <br />
                                    <b>Q: Do you charge on an hourly basis or fixed price only?
                                    <br />
                                    </b>A:Depending on the terms and conditions of the initial agreement, we may charge an hourly basis or project basis.
                                    <br />
                                    <br />
                                    <br />
                                    <br />
                                    <h3>General Queries</h3>
                                    <br />
                                    <br />
                                    <b>Q: Do you guarantee that the application will be approved in the App Store?
                                    <br />
                                    </b>A: We study the design and other specific requirements of the respective store guidelines and ensure that the app is developed in compliance with these guidelines. This guarantees the acceptance of the application by its respective app store. 
                                    <br />
                                    <br />
                                    <b>Q: I want an app for my business but don’t know where to start from?
                                    <br />
                                    </b>A: Just get in touch with our team and let us know what you are looking for and we’ll be more than happy to respond to your queries.
                                    <br />
                                    <br />
                                    <b>Q: I don’t work nearby so I should contact a local developer?
                                    <br />
                                    </b>A: You don’t need to be present in person for discussing the project with us as you can reach us through Skype, Emails, Phone, or any other online or offline mode of communication. In fact, we have worked for many offshore clients as well and provide impeccable service and support regardless of your location.
                                    <br />
                                    <br />
                                    <b>Q: Can I get to see a few examples of your work?
                                    <br />
                                    </b>A: You can refer to our portfolio section for details about our recent work.
                                    <br />
                                    <br />
                                    <b>Q: Is it possible to update the content of the app? If yes, how frequently can it be done?
                                    <br />
                                    </b>A: Yes, the content of the app can be updated anytime as per the need.



                                    <br />
                                    <br />
                                    <br />

                                    If you have any additional questions, please don't hesitate to <a href="contact.aspx">contact us</a>.<br />
                                    We would be happy to answer any queries you might have. 

                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

