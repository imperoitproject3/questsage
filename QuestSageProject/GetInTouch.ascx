﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GetInTouch.ascx.cs" Inherits="QuestSageProject.GetInTouch" %>
<%--<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>--%>
<script src="Content/Plugins/Validate/jquery.unobtrusive-ajax.min.js"></script>
<div class="footer-widget">
    <h3 class="widget-title" data-animate="fadeInUp" data-delay=".4">Get In Touch</h3>
    <div class="footer-form">
        <div class="form-row">
            <div class="col">
                <asp:TextBox ID="txtName" runat="server" CssClass="form-control animated fadeInUp" data-animate="fadeInUp" data-delay=".45" placeholder="Name"></asp:TextBox>
                <asp:RequiredFieldValidator Style="color: red;" runat="server" ControlToValidate="txtName" ErrorMessage="Please enter name" ValidationGroup="GetInTouch"></asp:RequiredFieldValidator>
            </div>
            <div class="col">
                <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control animated fadeInUp" data-animate="fadeInUp" data-delay=".5" placeholder="Email"></asp:TextBox>
                <asp:RequiredFieldValidator Style="color: red;" runat="server" ControlToValidate="txtEmail" ErrorMessage="Please enter email" ValidationGroup="GetInTouch"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="form-row">
            <div class="col">
                <asp:TextBox ID="txtContact" runat="server" CssClass="form-control animated fadeInUp" data-animate="fadeInUp" data-delay=".45" placeholder="Contact*"></asp:TextBox>
                <asp:RequiredFieldValidator Style="color: red;" runat="server" ControlToValidate="txtContact" ErrorMessage="Please enter contact" ValidationGroup="GetInTouch"></asp:RequiredFieldValidator>
            </div>
            <div class="col">
                <asp:DropDownList ID="ddlBudget" CssClass="form-control animated fadeInUp" runat="server">
                    <asp:ListItem Text="Budget (US $)" Value=""></asp:ListItem>
                    <asp:ListItem Text="Not Fixed" Value="Not Fixed"></asp:ListItem>
                    <asp:ListItem Text="Below 5k" Value="Below 5k"></asp:ListItem>
                    <asp:ListItem Text="5k-10k" Value="5k-10k"></asp:ListItem>
                    <asp:ListItem Text="10k-20k" Value="10k-20k"></asp:ListItem>
                    <asp:ListItem Text="20k-50k" Value="20k-50k"></asp:ListItem>
                    <asp:ListItem Text="Above 50k" Value="Above 50k"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

        <asp:TextBox ID="txtSubject" runat="server" CssClass="form-control animated fadeInUp" data-animate="fadeInUp" data-delay=".55" placeholder="Subject"></asp:TextBox>
        <asp:RequiredFieldValidator Style="color: red;" runat="server" ControlToValidate="txtSubject" ErrorMessage="Please enter subject" ValidationGroup="GetInTouch"></asp:RequiredFieldValidator>

        <asp:TextBox TextMode="MultiLine" CssClass="form-control w-100" Columns="20" Rows="4" ID="txtMessage" runat="server" data-animate="fadeInUp" data-delay=".6" placeholder="Message"></asp:TextBox>

        <asp:RequiredFieldValidator Style="color: red;" runat="server" ControlToValidate="txtMessage" ErrorMessage="Please enter subject" ValidationGroup="GetInTouch"></asp:RequiredFieldValidator>

        <div id="ReCaptchContainerGetInTouch"></div>
        <label id="lblMessage" runat="server" clientidmode="static"></label>

        <asp:Button ID="btnSubmitGetInTouch" runat="server" data-animate="fadeInUp" data-delay=".65" Text="Send" CssClass="btn btn-transparent" OnClientClick="fnValidateGetInTouch" OnClick="btnSubmitGetInTouch_Click" ValidationGroup="GetInTouch" />
    </div>
</div>

<script type="text/javascript">  
    <%--var istrue = false;
    $('#form1').submit(function () {
        var data = { 'captchaResponse': $('#g-recaptcha-response').val() };
        $.ajax({
            type: "GET",
            url: "default.aspx/IsReCaptchValid",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify(data),
            async: false,
            cache: false,
            success: function (data) {
                var returnedstring = data.d;
                if (returnedstring == false) {
                    jQuery('#lblMessage').css('color', 'red').html('Please check the Recaptcha');
                }
                else {
                    jQuery('#lblMessage').css('color', 'red').html(' ');
                    istrue = true;
                }
            },
            failure: function (errMsg) {
                alert(errMsg);
            },
            error: (error) => {
                console.log(JSON.stringify(error));
            }
        });
        return istrue;
    });

    var your_site_key = '<%= ConfigurationManager.AppSettings["SiteKey"]%>';
    var renderRecaptcha = function () {
        grecaptcha.render('ReCaptchContainerGetInTouch', {
            'sitekey': your_site_key,
            'callback': reCaptchaCallback,
            theme: 'light', //light or dark    
            type: 'image',// image or audio    
            size: 'normal'//normal or compact    
        });
    };

     var renderRecaptcha = function () {
        grecaptcha.render('ReCaptchContainer', {
            'sitekey': your_site_key,
            'callback': reCaptchaCallback,
            theme: 'light', //light or dark    
            type: 'image',// image or audio    
            size: 'normal'//normal or compact    
        });
    };

    var reCaptchaCallback = function (response) {
        if (response !== '') {
            jQuery('#lblMessage').css('color', 'green').html(' ');
            istrue = true;
        }
    };

    jQuery('button[type="button"]').click(function (e) {
        var message = 'Please check the Recaptcha';
        if (typeof (grecaptcha) != 'undefined') {
            var response = grecaptcha.getResponse();
            (response.length === 0) ? (message = 'Captcha verification failed') : (message = ' ');
        }
        jQuery('#lblMessage').html(message);
        jQuery('#lblMessage').css('color', (message.toLowerCase() == 'success!') ? "green" : "red");
    });--%>

</script>
<script type="text/javascript">
    function fnValidateGetInTouch() {        
    }
    function MessageSentSuccessfully() {
        alert('Message sent successfully!');
    }
    function FailedToSendMessage() {
        alert('Failed to send Message, please try again later');
    }
</script>
