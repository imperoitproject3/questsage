﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewMaster.master" AutoEventWireup="true" CodeBehind="iphone-game-development-company-india.aspx.cs" Inherits="QuestSageProject.iphone_game_development_company_india" %>

<%@ Register Src="~/Contact.ascx" TagPrefix="uc1" TagName="ContactUC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>iPhone Game Developers India, iPhone Game Development  Company</title>
    <meta name="description" content=" Quest Sage is a India based iPhone game development company working with startups & leading brands to develop cutting-edge iPhone game apps. " />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="title-bg-ios">
        <div class="container">
            <div class="page-title text-white text-center">
                <h1 data-animate="fadeInUp" data-delay="1.2">iPhone Game Development Company India </h1>
                <%--<h2 data-animate="fadeInUp" data-delay="1.4">Some Tagline here</h2>--%>
            </div>
        </div>
    </section>

    <section class="pt-70">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="single-post-page blog-left">
                        <div class="blog-details">

                            <div class="post-detail-content">

                                <h3>Innovative games that stand above the rest!</h3>
                                <p data-animate="fadeInUp" data-delay=".2">
                                    QuestSage specializes in iPhone game development introducing new strategies for game development. As a competent iPhone game development company in India & USA, we ensure that we can meet client requirements with inventive games that stand the test of time. Additionally, our team of iPhone game developers ensures that we develop games that entice and satisfy the gaming instincts of the entire gaming generation, suiting their interests and intelligence to the tee. 

                                   
                                </p>


                                <h3>Salient Features </h3>
                                <ul class="list-group">
                                    <li class="list-group-item">Trendy and Versatile Apps
We showcase iPhone game development with simple and advanced games for clients as well as end users 
                                    </li>
                                    <li class="list-group-item">Outstanding gaming apps
Our iPhone games constitute the best in the App store owing to the time, effort and intelligence in our ideation and development 
                                    </li>
                                    <li class="list-group-item">Multiple Gaming Genres
Our team of iPhone game developers in India & USA developed iPhone games for different genres of top-notch quality 
                                    </li>
                                    <li class="list-group-item">Beautiful graphics and awesome gameplay
Our apps boast of impeccable design and streamlined gameplay as we develop each of them with a deep understanding of the gaming psyche, aligned with the design trends too</li>
                                </ul>
                                <br />
                                <p>
                                    Creating modern iPhone games requires huge effort and skills in diverse ways. We focus on delivering an optimal user experience with impeccable gameplay, astounding characters and brilliant animation with latest tools and technologies. 
                                </p>
                                <br />
                                <div class="post-image">
                                    <img src="img/services/iOS_developer.png" alt="" data-animate="fadeInUp" data-delay=".1">
                                    <%--    <a href="contact.aspx">iPhone Game development services </a>--%>
                                </div>
                                <br />

                                <h3>Our Technological expertise</h3>
                                <ul class="list-group">
                                    <li class="list-group-item"><b>Location-based Games</b>
                                        <br />
                                        <br />
                                        We leverage the latest iPhone and iPad features for developing location-based games like Pokemon Go
                                    </li>
                                    <li class="list-group-item"><b>Unity Engine Support</b>
                                        <br />
                                        <br />
                                        Our team of iPhone app developers leverages Unity for developing iOS apps including loads of options and features for gamers to go berserk!
                                    </li>
                                    <li class="list-group-item"><b>Cocos2d Engine Assistance</b>
                                        <br />
                                        <br />
                                        Our team is well versed with Cocos2d in developing casual games with a 2D games engine that is superlative and often our preferred choice for creating unique mobile experiences.
                                    </li>
                                    <li class="list-group-item"><b>Unreal Engine Spectacle</b>
                                        <br />
                                        <br />
                                        We have the definitive expertise to develop games like Infinity Blade with beautiful imagery based on the Unreal gaming engine
                                    </li>
                                    <li class="list-group-item"><b>Adobe AIR </b>
                                        <br />
                                        <br />
                                        We leverage Adobe AIR to present compiled AIR-based games for our clients</li>

                                </ul>
                                <p>
                                    We realize your game idea and truly understand your end user needs. This is why our lean startup methodology has been able to test gaming features and functionality quickly in tandem with user expectations. 

                                </p>
                                <br />



                                <hr />
                                <div class="comment-form">
                                    <h3 class="widget-title" data-animate="fadeInUp" data-delay=".1">Contact us to know how QuestSage, the iphone game development company can ensure brilliant apps aligned with your vision and requirements.

                                    </h3>

                                    <uc1:ContactUC runat="server" ID="ContactUC" />

                                </div>
                                <%-- <section class="latest-news">
                                    <div class="container">
                                        <div class="row justify-content-center">
                                            <div class="col-xl-8 col-lg-8">
                                                <div class="section-title text-center">
                                                    <h2 data-animate="fadeInUp" data-delay=".1">Latest Developed iPhone Games</h2>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="single-post" data-animate="fadeInUp" data-delay=".1">
                                                    <a href="#">
                                                        <img src="img/post1.jpg" alt="">
                                                    </a>
                                                    <div class="post-content">

                                                        <h3><a href="#">App Name</a></h3>
                                                        <p>Basic Description</p>
                                                        <a href="#">Case Study<i class="fas fa-caret-right"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="single-post" data-animate="fadeInUp" data-delay=".3">
                                                    <a href="#">
                                                        <img src="img/post2.jpg" alt="">
                                                    </a>
                                                    <div class="post-content">

                                                        <h3><a href="#">App Name</a></h3>
                                                        <p>Basic Description</p>
                                                        <a href="#">Case Study<i class="fas fa-caret-right"></i></a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </section>--%>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="sidebar-widget" data-animate="fadeInUp" data-delay=".1">
                        <h3 class="widget-title" data-animate="fadeInUp" data-delay=".2">Client Speaks!

                        </h3>

                        <div class="why-us-video text-center text-white position-relative" data-animate="fadeInUp" data-delay=".1">

                            <img src="img/Video/bob.png" alt="">
                            <p>
                                <a class="pl-video" data-toggle="modal" data-target="#video-popup">
                                    <img src="img/play.png" alt="">
                                </a><span></span>
                            </p>


                        </div>
                    </div>
                    <br />
                    <br />

                    <aside>
                        <div class="sidebar-widget" data-animate="fadeInUp" data-delay=".1">
                            <h3 class="widget-title" data-animate="fadeInUp" data-delay=".2">Why Choose Us?
                            </h3>

                            <ul class="sidebar-list list-unstyled ">
                                <li class="list-group-item">Quality iPhone apps delivered
We do not compromise on quality standards in any of our apps 
                                </li>
                                <li class="list-group-item">On-Time Delivery and On Budget
We are committed to gaming excellence with seamlessness in our iPhone games without flouting committed duration. 
                                </li>
                                <li class="list-group-item">Consistent Communication
We communicate with our clients for getting timely inputs on a regular basis.
                                </li>
                                <li class="list-group-item">Complete Confidentiality
We are offering complete confidentiality about the information of the games that we develop and completely reliable.
                                </li>
                                <li class="list-group-item">100% Satisfaction Guaranteed
We guarantee outstanding satisfaction on par with client expectations as we test the games stringently 
                                </li>
                                <li class="list-group-item">Smart Team of iPhone Game Developers
Our experienced team of iPhone game developers offers technical finesse and legendary detail for our clients, and quite expert in attending to diverse domains too.</li>

                            </ul>


                        </div>
                        <br />
                        <br />

                        <h3 class="widget-title" data-animate="fadeInUp" data-delay=".2">Our iOS Game Development Process</h3>
                        <ul class="sidebar-list list-unstyled ">
                            <li class="list-group-item"><b>Requirement Gathering</b><br />
                                Evaluate the idea and collect requirements
                            </li>
                            <li class="list-group-item"><b>Plan Milestones</b><br />
                                Plan project milestones, allocating reasonable time for testing, design and development 
                            </li>
                            <li class="list-group-item"><b>Design the layout</b><br />
                                Choose the right design and gameplay based on audience 
                            </li>
                            <li class="list-group-item"><b>Game Development</b><br />
                                Develop the code for objects, layout, background, and animations 
                            </li>
                            <li class="list-group-item"><b>QA and Testing</b><br />
                                Ensure intensive quality assurance and testing for every part of the app 
                            </li>
                            <li class="list-group-item"><b>Final Delivery</b><br />
                                Integrate elements, levels and objects after proper testing, aligned with client’s vision
Contact us to know how QuestSage, the iphone game development company can ensure brilliant apps aligned with your vision and requirements.
                            </li>
                        </ul>

                    </aside>
                </div>
            </div>
        </div>
        <div class="modal fade" id="video-popup" tabindex="-1" role="dialog" aria-labelledby="videoLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <p>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </p>
                        <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Wfj1W2htBIs?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

