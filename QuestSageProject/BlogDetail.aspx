﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewMaster.Master" AutoEventWireup="true" CodeBehind="blogdetail.aspx.cs" Inherits="QuestSageProject.BlogDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="title-bg-dark">
        <div class="container">
            <div class="page-title text-white text-center">
                <h2 data-animate="fadeInUp" data-delay="1.2">Blog details</h2>
                <span data-animate="fadeInUp" data-delay="1.4"><a href="default.aspx">Home</a> / Blog details</span>
            </div>
        </div>
    </section>
    <section class="pt-120">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="single-post-page blog-left">
                        <div class="blog-details">
                            <div class="post-image">
                                <asp:Image ID="blogImage" runat="server" data-animate="fadeInUp" data-delay=".1" />
                                <a id="CategoryLink" runat="server" href="javascript:;">
                                    <asp:Literal ID="ltlCategoryName" runat="server"></asp:Literal>
                                </a>
                            </div>
                            <div class="post-detail-content">
                                <p class="post-info" data-animate="fadeInUp" data-delay=".1">
                                    Posted on 
                                    <a href="javascript:;">
                                        <asp:Literal ID="ltlPostedOn" runat="server"></asp:Literal></a> by <a href="javascript:;">
                                            <asp:Literal ID="ltlPostedBy" runat="server"></asp:Literal></a>
                                </p>

                                <h2 data-animate="fadeInUp" data-delay=".15">
                                    <asp:Literal ID="ltlBlogTitle" runat="server"></asp:Literal></h2>

                                <div id="divBlogBody" runat="server">
                                </div>
                            </div>
                        </div>
                        <div class="tag-cat-soc" data-animate="fadeInUp" data-delay=".1">
                            <div class="row align-items-center">
                                <div class="col-md-8 col-sm-7">
                                    <ul class="tag-cat karla list-inline">
                                        <li><i class="fas fa-tags"></i><a href="javascript:;">
                                            <asp:Literal ID="ltlTagName" runat="server"></asp:Literal></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <aside>
                        <div class="sidebar-widget" data-animate="fadeInUp" data-delay=".1" id="divRecentPost" runat="server">
                            <h3 class="widget-title" data-animate="fadeInUp" data-delay=".2">Recent Post</h3>
                            <ul class="sidebar-list list-unstyled">

                                <asp:Repeater ID="rptRecentPost" runat="server">
                                    <ItemTemplate>
                                        <li data-animate="fadeInUp" data-delay=".3">
                                            <a href="<%# Page.GetRouteUrl("Blog",new { title= ToTitle(Eval("Title").ToString()), id=Eval("BlogId") }) %>"><%# Eval("Title") %></a></li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                        </div>
                        <br />
                        <br />
                        <div class="sidebar-widget" data-animate="fadeInUp" data-delay=".1">
                            <h3 class="widget-title" data-animate="fadeInUp" data-delay=".2">Categories</h3>
                            <ul class="sidebar-list karla list-unstyled">
                                <asp:Repeater ID="rptCategories" runat="server">
                                    <ItemTemplate>
                                        <li data-animate="fadeInUp" data-delay=".3" class="category<%# Eval("Id") %>">
                                            <a href="<%# Page.ResolveUrl("blog.aspx?CategoryId="+Eval("Id")) %>"><%# Eval("Name") %> <span>(<%# Eval("BlogCount") %>)</span></a>

                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </section>
</asp:Content>