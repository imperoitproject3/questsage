﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.Web.DynamicData;
using QuestSageProject.DAL;
using System.IO;
using System.Web.Services;
using System.Net;
using Newtonsoft.Json.Linq;

namespace QuestSageProject.DynamicData.FieldTemplates
{
    public partial class ContactUs : System.Web.DynamicData.FieldTemplateUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmitInquiry_Click(object sender, EventArgs e)
        {
            try
            {
                QuestsageEntities _ctx = new QuestsageEntities();
                Inquiry data = new Inquiry
                {
                    Name = txtName.Text,
                    Email = txtEmail.Text,
                    Country = txtCountry.Text,
                    ContactNumber = txtContact.Text,
                    Skype = txtSkype.Text,
                    Budget = ddlBudget.SelectedValue,
                    NDARequired = chkNDARequired.Checked,
                    ProjectDetails = txtProjectDetail.Text
                };

                if (fileInquiry != null && fileInquiry.HasFile)
                {
                    string ogFileName = fileInquiry.FileName.Trim('\"');
                    string shortGuid = Guid.NewGuid().ToString();
                    var thisFileName = shortGuid + Path.GetExtension(ogFileName);

                    string InquiryDocsPath = Server.MapPath("~/Files/InquiryDocs/");
                    fileInquiry.SaveAs(Path.Combine(InquiryDocsPath, thisFileName));
                    data.DocumentFileName = thisFileName;
                }
                _ctx.Inquiry.Add(data);
                _ctx.SaveChanges();

                txtEmail.Text = txtName.Text = txtEmail.Text = txtContact.Text = txtCountry.Text = txtSkype.Text = txtProjectDetail.Text = string.Empty;

                Page.ClientScript.RegisterStartupScript(this.GetType(), "Success", "InquirySubmittedSuccessfully()", true);
            }
            catch (Exception)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Failed", "FailedToSendInquiry()", true);
            }
        }

        [WebMethod]
        public static bool IsReCaptchValid(string captchaResponse)
        {
            string secretKey = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SecretKey");
            var result = false;
            var apiUrl = "https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}";
            var requestUri = string.Format(apiUrl, secretKey, captchaResponse);
            var request = (HttpWebRequest)WebRequest.Create(requestUri);

            using (WebResponse response = request.GetResponse())
            {
                using (StreamReader stream = new StreamReader(response.GetResponseStream()))
                {
                    JObject jResponse = JObject.Parse(stream.ReadToEnd());
                    var isSuccess = jResponse.Value<bool>("success");
                    result = (isSuccess) ? true : false;
                }
            }
            return result;
        }
    }
}
