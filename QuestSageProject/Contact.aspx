﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewMaster.master" AutoEventWireup="true" CodeBehind="contact.aspx.cs" Inherits="QuestSageProject.Contact1" %>

<%@ Register Src="~/ContactUs.ascx" TagPrefix="ucInquiry" TagName="ContactUC" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .contact-content h2 {
            margin-bottom: 4px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="title-bg-contact">
        <div class="container">
            <div class="page-title text-white text-center">
                <h1 data-animate="fadeInUp" data-delay="1.2">Let's Talk Ideas </h1>
                <%--<h2 data-animate="fadeInUp" data-delay="1.4">Some Tagline here</h2>--%>
            </div>
        </div>
    </section>
    <section class="pt-120 pb-120">
        <div class="container">
            <div class="row">
                <div class="col-lg-6" data-animate="fadeInUp" data-delay=".1">
                    <div class="contact-content">
                        <h2 data-animate="fadeInUp" data-delay=".1">FREE QUOTE</h2>
                        <p data-animate="fadeInUp" data-delay=".3">Based on your requirements, we would be happy to share non-obligatory quote for your app’s concept.</p>
                        <p>Please fill in the form on the right and we will connect with you very soon :)</p>
                        <h2 data-animate="fadeInUp" data-delay=".1">FREE 30 MINUTES CONSULTANCY:</h2>
                        <p data-animate="fadeInUp" data-delay=".3">If you have an idea and would like to discuss it with us, please contact us and we would be happy to arrange FREE consultation of 30 minutes whereby we can give you our feedback and reviews on your concept and help you out with few details based on our experience and latest market trends.</p>
                        <h3 data-animate="fadeInUp" data-delay=".1">QuestSage IT Solution</h3>
                        <p data-animate="fadeInUp" data-delay=".3">
                            B1-301, Westgate Business bay,
                            S G highway, Ahmedabad,<br />
                            Gujarat, India - 380051
                        </p>
                        <p><span>Phone :</span> <a href="tel:+919825-108987">(+91) 9825-108987</a></p>
                        <p><span>Email :</span> <a href="mailto:info@questsage.com">info@questsage.com</a></p>
                    </div>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3672.7070559447775!2d72.49676501411751!3d22.997797384965672!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x395e9add24c1abaf%3A0x416bcc7b54495c45!2sWestgate+Business+Bay+by+True+Value!5e0!3m2!1sen!2sin!4v1537168166315" width="500" height="450" frameborder="0" style="border: 0" allowfullscreen></iframe>
                </div>
                <div class="col-lg-6">
                    <div class="contact-form">
                        <ucInquiry:ContactUC runat="server" ID="ContactUC" />
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
