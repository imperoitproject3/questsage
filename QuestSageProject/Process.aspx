﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewMaster.master" AutoEventWireup="true" CodeBehind="process.aspx.cs" Inherits="QuestSageProject.Process" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="title-bg-process">
        <div class="container">
            <div class="page-title text-white text-center">
                <h1 data-animate="fadeInUp" data-delay="1.2">Our Process </h1>
                <%--<h2 data-animate="fadeInUp" data-delay="1.4">Some Tagline here</h2>--%>
            </div>
        </div>
    </section>
    <section class="pt-70">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="single-post-page blog-left">
                        <div class="blog-details">
                            <div class="post-image">
                                <img src="img/services/process.png" alt="" data-animate="fadeInUp" data-delay=".1"><br />
                                <%--<a href="contact.aspx">Our process for app development</a>--%>
                            </div>
                            <div class="post-detail-content">
                                <p data-animate="fadeInUp" data-delay=".2">
                                    Here at QuestSage IT Services, we follow a seamless, dedicated, and client-centric approach to provide them superior quality apps. Client satisfaction and excellent execution form the backbone of all our processes. The steps involved in the process can be classified mainly into
                                </p>

                                <h3>1. Outlining the idea and collecting all relevant information</h3>
                                <p>
                                    You must be having an idea of what you want your app to look like. Just give us the basic information about what you expect and let us do in-depth research and planning to conceptualize your idea and define its scope. We will then prepare the proposal and requirements and provide you with the estimate for your approval.
                                </p>
                                <h3>2. Internal analysis and prototyping</h3>
                                <p>
                                    Once the project is approved, the pre-engineering process involving the comprehensive internal analysis, concept strategy, root-level construction, and technical consultation is initiated. The wireframe, prototype, or storyboard can then be used to select the right development platforms and structures. .
                                </p>
                                <h3>3. Designing the UI and UX</h3>
                                <p>
                                    The functionality and design of your app are dependent on the design and the intuitive, friendly, intriguing, and addictive interface we provide to the users. Thus, by using the latest UI features that are relevant to your app and making it emotional with suitable colours, we provide the best experience to the users..
                                </p>
                                <h3>4. Development within the stipulated timeframe</h3>
                                <p>
                                    The next stage involves releasing the plan and our experienced and competent developers pair the right technology to provide you with the perfect solution within the stipulated timeframe. Full stack backend development and coding are optimized for updates on the routine status..
                                </p>
                                <h3>5. Testing and quality assurance</h3>
                                <p>
                                    Static and automated tests are performed to eliminate the bugs in the app by bringing the beta version of the app into action. After testing in various environments and conditions, real-time user testing is done to collect feedback and make the final adjustments..
                                </p>
                                <h3>6. Deployment and launch followed by post-launch support and maintenance</h3>
                                <p>
                                    The finished masterpiece is then deployed on respective app stores and released into the live environment. Our work here is to ensure the integrity and provide the needed post-deployment maintenance and support.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="sidebar-widget" data-animate="fadeInUp" data-delay=".1">
                        <h3 class="widget-title" data-animate="fadeInUp" data-delay=".2">Client Speaks!
                        </h3>
                        <div class="why-us-video text-center text-white position-relative" data-animate="fadeInUp" data-delay=".1">
                            <img src="img/Video/bob.png" alt="">
                            <p>
                                 <a class="pl-video" data-toggle="modal" data-target="#video-popup">
                                    <img src="img/play.png" alt="">
                                </a><span></span>
                            </p>
                        </div>
                    </div>
                    <br />
                    <br />
                    <aside>
                        <div class="sidebar-widget" data-animate="fadeInUp" data-delay=".1">
                            <h3 class="widget-title" data-animate="fadeInUp" data-delay=".2">  Development Services</h3>
                            <ul class="list-group">
                                <li class="list-group-item"><a href="android-app-development-company-india.aspx">Android App Development</a></li>
                                <li class="list-group-item"><a href="iphone-app-dvelopment-company-india">iPhone App Development</a></li>
                                <li class="list-group-item"><a href="augmented-reality-app-development-company-india.aspx">Augmented Reality App</a></li>
                                <li class="list-group-item"><a href="wearable-app-development-company-india.aspx">Wearable App</a></li>
                                <li class="list-group-item"><a href="virtual-reality-app-development-company-india.aspx">Virtual Reality </a></li>
                                <li class="list-group-item"><a href="iot-app-development-company-india.aspx">IOT Apps</a></li>
                                <li class="list-group-item"><a href="chatbot-development-company-india.aspx">Chatbot App</a></li>
                                <li class="list-group-item"><a href="mobile-app-design-company-india.aspx">Mobile App Design</a></li>
                                <li class="list-group-item"><a href="cloud-app-development-company-india.aspx">Cloud App Development</a></li>
                                <li class="list-group-item"><a href="beacon-app-development-company-india.aspx">Beacon App Development</a></li>

                                <li class="list-group-item"><a href="virtual-reality-game-development-company-india.aspx">Virtual Reality Game</a></li>
                                <li class="list-group-item"><a href="iphone-game-development-company-india.aspx">iPhone Game Development</a></li>
                                <li class="list-group-item"><a href="android-game-development-company-india.aspx">Android Game Development</a></li>
                                <li class="list-group-item"><a href="unity-3d-game-development-company-india.aspx">Unity 3D Game</a></li>
                                <li class="list-group-item"><a href="hire-android-app-developers-india.aspx">Hire Android Developers</a></li>
                                <li class="list-group-item"><a href="hire-iphone-app-developers-india.aspx">Hire iPhone Developers</a></li>
                                <li class="list-group-item"><a href="hire-cross-platform-developers-india.aspx">Hire Cross Platform Developers</a></li>
                                <li class="list-group-item"><a href="xamarin-app-development-company-india.aspx">Xamarin App Development</a></li>
                                <li class="list-group-item"><a href="phonegap-app-development-company-india.aspx">Phonegap App Development</a></li>
                                <li class="list-group-item"><a href="titanium-app-development-company-india.aspx">Titanium App Development </a></li>

                            </ul>
                            <br />
                        </div>


                    </aside>
                </div>
            </div>
        </div>
         <div class="modal fade" id="video-popup" tabindex="-1" role="dialog" aria-labelledby="videoLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <p>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </p>
                          <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Wfj1W2htBIs?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

