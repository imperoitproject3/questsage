﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewMaster.master" AutoEventWireup="true" CodeBehind="about.aspx.cs" Inherits="QuestSageProject.About" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="title-bg-darkabout">
        <div class="container">
            <div class="page-title text-white text-center">
                <h1 data-animate="fadeInUp" data-delay="1.2">About</h1>
            </div>
        </div>
    </section>
    <section class="about-us pt-70 pb-120">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="about-us-title text-left">
                        <span data-animate="fadeInUp" data-delay=".2">QuestSage IT Services is a comprehensive mobile app development services company, which varied mobile app development services in India for several domains. 
                        </span>
                    </div>
                    <p data-animate="fadeInUp" data-delay=".4">
                        With a strong background in development solutions and 100% client retention rate, QuestSage IT Services is geared towards providing quality in its deliverables without compromise. In recent years, the company has grown from strength to strength, while app programmers in India having implemented numerous projects successfully in varied domains. Our Android app development services in India are geared to introduce huge security measures and robust inventiveness in each of the apps. Similarly, our iPhone app development services in India include the development of business apps, enterprises apps, games, utility apps and many more.
                    </p>
                </div>
                <div class="col-lg-6">
                    <div class="why-us-video why-us-video-sticky-tape text-center text-white position-relative" data-animate="fadeInUp" data-delay=".1">
                        <div class="sticky-tape">
                            <img src="<%= Page.ResolveUrl("~/img/about-video-bg.jpg") %>" alt="">
                            <p>
                                <a class="pl-video" data-toggle="modal" data-target="#video-popup">
                                    <img src="<%= Page.ResolveUrl("~/img/play.png") %>" alt="">
                                   <%-- <span>You Will Buy After Watching</span>--%></a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="pt-70">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="blog">
                        <div class="row">
                            <div class="col-md-12" data-animate="fadeInUp" data-delay=".1">
                                <img src="<%= Page.ResolveUrl("~/img/C_A_Work&Development.png") %>" alt="">
                            </div>
                            <div class="col-md-6" data-animate="fadeInUp" data-delay=".1">
                                <div class="single-post">



                                    <div class="post-content">
                                        <h3>Work Methodology</h3>
                                        <p>
                                            The company adheres to a stringent methodology that follows a tried-and-tested procedure that has done wonders time and again.
                                            <br />
                                            We also have diverse business alliances with other companies and are IT Consultant partners for diverse organizations.
                                        </p>
                                        <ul class="list-group">
                                            <li class="list-group-item">We conduct a thorough analysis of the requirements and assess the business needs of each solution</li>
                                            <li class="list-group-item">We design the solution, aligned with the requirements and the business image
                                            </li>
                                            <li class="list-group-item">We develop relevant solutions based on diverse technologies based on the design, aligned with business needs for the present and the future
                                            </li>
                                            <li class="list-group-item">None of our solutions skips Quality Control, and our own quality assurance process makes sure that the bugs are weeded out while the navigation is on point
                                            </li>
                                            <li class="list-group-item">We deploy and implement the software solutions on the client’s system, aiding them with every element of the process
                                            </li>
                                            <li class="list-group-item">Our prompt support and maintenance services are assured and available on time for every developed solution


                                            </li>

                                        </ul>


                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" data-animate="fadeInUp" data-delay=".2">
                                <div class="single-post">

                                    <%--                                        <img src="<%= Page.ResolveUrl("~/img/post4.jpg") %>" alt="">--%>

                                    <div class="post-content">
                                        <h3>Our Development Strategy
                                        </h3>
                                        <p>
                                            As a veteran mobile application development company in USA, we make sure that we accomplish your individual demands and fulfill your business needs. With advanced tools and technologies, our team can create customized mobile applications that align seamlessly with your present processes along with domain-specific functions. 
                                            <br />
                                            <br />
                                            It is clear that the immense demand of mobile apps for both platforms in varied domains have been increasing steadily. The introduction of AR and VR has also led to new ideas and apps which are keen to partner with effective and efficient mobile app development companies in India, especially to aid them in the process. To cope with their specific domain-related requirements, QuestSage IT Services can be the perfect catalyst and partner to design and develop inventive mobile applications that would always stand the test of time.
 <br />
                                            <br />
                                            We offer comprehensive mobile application development services in India for iPhone, iPad and Android. Our team of mobile app programmers for hire is well versed in the development process, owing to their expertise and experience in working with varied domains & industries. The retention of a large number of clients is a testimony to the success that we have had in the field.
                                             <br />
                                            <br />
                                            Unlike other top mobile app development companies in India, QuestSage IT Services does not stick to a particular strategy or approach to the development or testing tasks. With a huge team of analysts, developers, and designers, we ensure that each strategy and plan is effective and efficient to reap adroitly managed solutions, as required by clients. 
                                            <br />
                                            <br />

                                            <br />

                                        </p>

                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="bg-light bg-rotate position-relative">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-8">
                    <div class="section-title text-center">
                        <h2 data-animate="fadeInUp" data-delay=".1">Our Brilliant Team Members</h2>
                     <%--   <p data-animate="fadeInUp" data-delay=".2">Client satisfaction is our goal and we value our patronage. Here is what our clients say.</p>--%>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center"> 
                <div class="col-lg-2 col-md-2 col-sm-6">
                    <div class="single-member" data-animate="fadeInUp" data-delay=".3">
                        <img width="100%" src="<%= Page.ResolveUrl("~/img/member1.jpg") %>" alt="">
                        <div class="member-info bg-dark">
                            <h4>Raza Noorani</h4>
                            <span><small> CEO </small> </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-6">
                    <div class="single-member" data-animate="fadeInUp" data-delay=".4">
                        <img width="100%" src="<%= Page.ResolveUrl("~/img/member2.jpg") %>" alt="">
                        <div class="member-info bg-dark">
                            <h4>Sadik Jamani</h4>
                            <span><small> Project Manager</small> </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-6">
                    <div class="single-member" data-animate="fadeInUp" data-delay=".5">
                        <img width="100%" src="<%= Page.ResolveUrl("~/img/member3.jpg") %>" alt="">
                        <div class="member-info bg-dark">
                            <h4>Ravi Khunt</h4>
                            <span><small> Sr .Net Developer</small> </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-6">
                    <div class="single-member" data-animate="fadeInUp" data-delay=".6">
                        <img width="100%" src="<%= Page.ResolveUrl("~/img/member4.jpg") %>" alt="">
                        <div class="member-info bg-dark">
                            <h4>Dharmesh Pandya</h4>
                            <span><small> Sr Android Developer</small></span>
                        </div>
                    </div>
                </div>
                  <div class="col-lg-2 col-md-2 col-sm-6">
                    <div class="single-member" data-animate="fadeInUp" data-delay=".6">
                        <img width="100%" src="<%= Page.ResolveUrl("~/img/member5.jpg") %>" alt="">
                        <div class="member-info bg-dark">
                            <h4>Arpit Dhamane</h4>
                            <span><small> Sr iOS Developer</small> </span>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <div class="modal fade" id="video-popup" tabindex="-1" role="dialog" aria-labelledby="videoLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <p>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </p>
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/J3MtHo-nvHQ?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
