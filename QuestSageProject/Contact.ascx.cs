﻿using QuestSageProject.DAL;
using System;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;

namespace QuestSageProject
{
    public partial class Contact : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmitInquiry_Click(object sender, EventArgs e)
        {
            try
            {
                QuestsageEntities _ctx = new QuestsageEntities();
                Inquiry data = new Inquiry
                {
                    Name = txtName.Text,
                    Email = txtEmail.Text,
                    Country = txtCountry.Text,
                    ContactNumber = txtContact.Text,
                    Skype = txtSkype.Text,
                    Budget = ddlBudget.SelectedValue,
                    NDARequired = chkNDARequired.Checked,
                    ProjectDetails = txtProjectDetail.Text
                };
                string FilePath = "";
                if (fileInquiry != null && fileInquiry.HasFile)
                {
                    string ogFileName = fileInquiry.FileName.Trim('\"');
                    string shortGuid = Guid.NewGuid().ToString();
                    var thisFileName = shortGuid + Path.GetExtension(ogFileName);

                    string InquiryDocsPath = Server.MapPath("~/Files/InquiryDocs/");
                    fileInquiry.SaveAs(Path.Combine(InquiryDocsPath, thisFileName));
                    data.DocumentFileName = thisFileName;
                    FilePath = InquiryDocsPath + "\\" + thisFileName;
                }
                _ctx.Inquiry.Add(data);
                _ctx.SaveChanges();

                StringBuilder ContentForAdmin = new StringBuilder();
                ContentForAdmin.Append("Person With Name: " + data.Name + " has contacted you on your website www.squareroot.ie");
                ContentForAdmin.Append("\n\n\r");

                ContentForAdmin.Append("\n\n\r");
                ContentForAdmin.Append("IT Consultancy Required  " + data.NDARequired);
                ContentForAdmin.Append("His Contact Number is: " + data.ContactNumber);
                ContentForAdmin.Append("\n\n\r");
                ContentForAdmin.Append("His Email Address is: " + data.Email);
                ContentForAdmin.Append("\n\n\r");
                ContentForAdmin.Append("His Skype id is: " + data.Skype);
                ContentForAdmin.Append("\n\n\r");
                ContentForAdmin.Append("His ProjectDetails is : " + data.ProjectDetails);
                ContentForAdmin.Append("\n\n\r");
                ContentForAdmin.Append("His Budget is : " + data.Budget);
                ContentForAdmin.Append("\n\n\r");
                ContentForAdmin.Append("Thank You");
                
                SendEmail(ContentForAdmin, "notification@imperoit.com", "raza@questsage.com", "juned.ghanchi@gmail.com", "", "New Inquiry on www.questsage.com", FilePath);

                txtEmail.Text = txtName.Text = txtEmail.Text = txtContact.Text = txtCountry.Text = txtSkype.Text = txtProjectDetail.Text = string.Empty;

                Page.ClientScript.RegisterStartupScript(this.GetType(), "Success", "InquirySubmittedSuccessfully()", true);
            }
            catch (Exception)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Failed", "FailedToSendInquiry()", true);
            }
        }

        public void SendEmail(StringBuilder Mailcontent, string From, string To, string Cc, string Bcc, string Subject, string FulResume)
        {
            //To = Cc = Bcc = "jafar.imperoit@gmail.com";
            if (HttpContext.Current.Request.Url.ToString().IndexOf("localhost") > 0) return;
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.zoho.com");
            mail.From = new MailAddress(From);
            mail.To.Add(To);
            mail.CC.Add(Cc);
            mail.Subject = Subject;
            mail.Body = Mailcontent.ToString();
            mail.IsBodyHtml = true;
            if (FulResume != "")
            {

                System.Net.Mail.Attachment attachment;
                attachment = new System.Net.Mail.Attachment(FulResume);
                mail.Attachments.Add(attachment);
            }
            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential(From, "8mFS@bdeY");
            SmtpServer.EnableSsl = true;

            SmtpServer.Send(mail);


        }
    }
}