﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewMaster.master" AutoEventWireup="true" CodeBehind="android-game-development-company-india.aspx.cs" Inherits="QuestSageProject.android_game_development_company_india" %>
<%@ Register Src="~/Contact.ascx" TagPrefix="uc1" TagName="ContactUC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title> Android Game Developers India, Android Game Development  Company</title>
<meta name="description" content="QuestSage is an leading Android game development company in India, Expertise in 2D & 3D Android mobile game development services. "/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="title-bg-game">
        <div class="container">
            <div class="page-title text-white text-center">
                <h1 data-animate="fadeInUp" data-delay="1.2">Android Game Development </h1>
                <%--<h2 data-animate="fadeInUp" data-delay="1.4">Encounter convenient, value-based games – experience fun, progress, and accomplishment</h2>--%>
            </div>
        </div>
    </section>
    <section class="pt-70">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="single-post-page blog-left">
                        <div class="blog-details">
                           
                            <div class="post-detail-content">
                                <p data-animate="fadeInUp" data-delay=".2">
                                    QuestSage is one Android game development company in India & USA well versed in developing diverse Android games. Our team of Android game developers and designers are keen to leverage the Android experience to another level.<br />
                                </p>
                                <br />
                                <b>Innovative Android games… Unleashed!</b><br />
                                QuestSage specializes in Android game development services that meet comprehensive client requirements, which will define the future.

                                    <br />
                                <br />
                                <b>Entertaining and Educational</b>
                                <br />
                                We develop Android games that are entertaining and educational for gamers and aligned with present trends. 
                                    <br />
                                <br />
                                <b>Versatile and Vivid</b>
                                <br />
                                We display immense versatility and vividness in our offerings leveraging platform features an attractive design to the fore.
                                    <br />
                                <br />
                                <b>Premium quality </b>
                                <br />
                                Our Android game developers in India & USA ensure premium quality games that are addictive and brilliant in gameplay with expertise involved in the development process
                                    <br />
                                <br />
                                <b>Beyond Market Trends </b>
                                <br />
                                We defy the present trends and go beyond client expectations in developing Android games that are innovative and interesting in more ways than one. 
                                    <br />
                                <br />
                                <b>Multiple Gaming Genres</b>
                                <br />
                                Our team of Android game developers can develop games for different gaming genres that include action, adventure, puzzle, etc.    
                                    <br />
                                <br />
                                <b>Attractive graphics and seamless gameplay</b>
                                <br />
                                We blend brilliant design with seamless gameplay perfectly with attractive games showcased in our portfolio, testimony to our expertise.
                                    <br />
                                <br />
                                 <div class="post-image">
                                <img src="<%= Page.ResolveUrl("~/img/services/Android_developer.png") %>" alt="" data-animate="fadeInUp" data-delay=".1">
                           <%--     <a href="contact.aspx">Intelligent, Innovative Android Games - 
                                        Android games that are ahead of the curve… </a>--%>
                            </div>

                                <hr />
                                <br />
                                <h3>Why Choose QuestSage?</h3>
                                <p>
                                    QuestSage, a mobile app development company, has a magnificent legacy of developing outstanding Android games with a strong clientele who have depended on our team of Android game developers for success
                                </p>

                                <b>Quality and Scalability</b><br />
                                We refrain from compromising on any measure of quality standards, adhering to quality checks and scalable on different fronts too<br />
                                <br />
                                <b>On-Time and On-Budget</b><br />
                                We deliver Android games on-time without compromise of time, effort and precision, even managing the app development process within budget.
                                <br />
                                <br />
                                <b>Consistent Communication </b>
                                <br />
                                We communicate with clients regularly right throughout the entire Android game development phase.
                                <br />
                                <br />
                                <b>Complete Security and Confidentiality</b><br />
                                Our team of Android game developers ensures complete integrity and honesty in keeping all game details confidential, keeping client information secured in all manners.
                                <br />
                                <br />
                                <b>Satisfaction Guaranteed</b><br />
                                We ensure complete satisfaction for clients always striving to meet the high expectations.
                                <br />
                                <br />
                                <b>Efficient and Expert Developers</b><br />
                                Our technical team is brilliant in Android game development process since they are experienced enough in dealing with multiple clients and domains.

                               
                                <br />

                                <hr />
                                <div class="comment-form">
                                    <h3 class="widget-title" data-animate="fadeInUp" data-delay=".1">Contact us to know more about our Android Game development process and how our expert Android game developers can deliver the goods!
                                    </h3>

                                    <uc1:ContactUC runat="server" ID="ContactUC" />

                                </div>
                               <%-- <section class="latest-news">
                                    <div class="container">
                                        <div class="row justify-content-center">
                                            <div class="col-xl-8 col-lg-8">
                                                <div class="section-title text-center">
                                                    <h2 data-animate="fadeInUp" data-delay=".1">Latest Developed Android Games</h2>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="single-post" data-animate="fadeInUp" data-delay=".1">
                                                    <a href="#">
                                                        <img src="<%= Page.ResolveUrl("~/img/post1.jpg") %>" alt="">
                                                    </a>
                                                    <div class="post-content">

                                                        <h3><a href="#">App Name</a></h3>
                                                        <p>Basic Description</p>
                                                        <a href="#">Case Study<i class="fas fa-caret-right"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="single-post" data-animate="fadeInUp" data-delay=".3">
                                                    <a href="#">
                                                        <img src="<%= Page.ResolveUrl("~/img/post2.jpg") %>" alt="">
                                                    </a>
                                                    <div class="post-content">

                                                        <h3><a href="#">App Name</a></h3>
                                                        <p>Basic Description</p>
                                                        <a href="#">Case Study<i class="fas fa-caret-right"></i></a>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </section>--%>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="sidebar-widget" data-animate="fadeInUp" data-delay=".1">
                        <h3 class="widget-title" data-animate="fadeInUp" data-delay=".2">Client Speaks!

                        </h3>

                        <div class="why-us-video text-center text-white position-relative" data-animate="fadeInUp" data-delay=".1">

                            <img src="<%= Page.ResolveUrl("~/img/Video/bob.png") %>" alt="">
                            <p>
                                 <a class="pl-video" data-toggle="modal" data-target="#video-popup">
                                    <img src="<%= Page.ResolveUrl("~/img/play.png") %>" alt="">
                                </a><span></span>
                            </p>


                        </div>
                    </div>
                    <br />
                    <br />

                    <aside>
                        <div class="sidebar-widget" data-animate="fadeInUp" data-delay=".1">
                            <h3 class="widget-title" data-animate="fadeInUp" data-delay=".2">Our Android Game Development Approach

                               



                            </h3>

                            <p>Witness our development process for Android games with lots of attention to gameplay and their intrinsic features. <br /> Our process ensures our consistent success with all sorts of clients.</p>
                            <ul class="sidebar-list list-unstyled ">

                                <li class="list-group-item"><b>Requirement Analysis</b><br />
                                    <br />
                                    We evaluate and analyze the requirements based on the concept and idea while also evaluating the gaming levels and their complexity.  </li>
                                </li><li class="list-group-item"><b>Timely Milestones</b><br />
                                    <br />
                                    We ensure that the milestones are set for every module with a consistent process of QA and testing in between other development stages
                                </li>
                                <li class="list-group-item"><b>Sensational Game Design </b>
                                    <br />
                                    <br />
                                    We help our clients by delivering an outstanding game design blended with gaming elements for the young audience 
                                </li>
                                <li class="list-group-item"><b>QA and Testing</b><br />
                                    <br />
                                    By assessing each gaming feature, we perform thorough QA and testing so that bugs are removed on a consistent basis with absolutely no discrepancies by the end
                                </li>
                                <li class="list-group-item"><b>Final Seamless Delivery</b><br />
                                    <br />
                                    We bring multiple elements together with levels, objects and gameplay seamlessly with the game concept ensuring seamless delivery.
  
                                </li>

                            </ul>

                        </div>
                        <br />
                        <br />


                    </aside>
                </div>
            </div>
        </div>
           <div class="modal fade" id="video-popup" tabindex="-1" role="dialog" aria-labelledby="videoLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <p>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </p>
                      <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Wfj1W2htBIs?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

