﻿function EnsureAlphaNumericOnly(strTextboxID) {
    var objTextbox = document.getElementById(strTextboxID);
    if (objTextbox.value.match(/[^a-zA-Z0-9 ]/g)) {
        objTextbox.value = objTextbox.value.replace(/[^a-zA-Z0-9 ]/g, '');
    }
    return false;
}
function EnsureNumeric(event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
    return true
}
function EnsureDecimal(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode == 118)
        return true;
    else if (charCode == 44) {
        var src = (typeof evt.target != 'undefined') ? evt.target : evt.srcElement;
        var myArray = src.value.split('');
        for (i = 0; i < myArray.length; i++) {
            if (myArray[i] == ',' || myArray[i] == ".") {
                return false;
            }
        }
        if (myArray.length == 0)
            return false;
    }
    else if (charCode == 46) {
        var src = (typeof evt.target != 'undefined') ? evt.target : evt.srcElement;
        var myArray = src.value.split('');
        for (i = 0; i < myArray.length; i++) {
            if (myArray[i] == '.') {
                return false;
            }
        }
        if (myArray.length == 0)
            return false;
    }
    else if (charCode > 31 && (charCode < 46 || charCode > 57))
        return false;
    return true;
}