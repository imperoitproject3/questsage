﻿
function SaveCropedImage(saveFilePath, hdnImageName, postUrl) {
    StartBlockPage();
    var $image = $("#cropimage_" + hdnImageName);
    var result = $image.cropper("getCroppedCanvas");
    var imgData = result.toDataURL('image/png');

    var extension = $("#fileImage_" + hdnImageName).val().split('.').pop();
    var prevImageName = $("#prev_" + hdnImageName).val();

    var model = {
        imgBase64String: imgData,
        saveFilePath: saveFilePath,
        extension: extension,
        previousImageName: prevImageName
    };

    $.ajax({
        type: 'POST',
        url: postUrl,
        data: JSON.stringify(model),
        contentType: 'application/json; charset=utf-8',
        dataType: 'JSON',
        success: function (resultResponse) {
            $("#solid-alerts").show();
            $('#solid-alerts #message').text(resultResponse.Message);
            if (resultResponse.Status == 1) {
                $("#solid-alerts .alert").addClass("bg-primary");
                $('#ImageName').val(resultResponse.Result);

                var div = '<div class="col-lg-3 col-md-6 col-xs-12">';
                div += '<img class="img-thumbnail img-fluid img-custom-size" data-new="1" data-imagename="' + resultResponse.Result + '" src="' + imgData + '"/><br/><br/>';                
                div += '</div>';

                $('#divUpload').html(div);
                $('#spanErrMultiUpload').hide();
            }
            else {
                $("#solid-alerts .alert").addClass("bg-warning");
                toastr.error(resultResponse.Message, 'Error !');
            }
            $("#Modal_" + hdnImageName).modal('hide');
            CloseBlockPage();
        },
        error: function (e) {
            $("#solid-alerts .alert").addClass("bg-danger");
            $("#solid-alerts").show();
            $('#solid-alerts #message').text(e.status + "," + e.statusText);
            CloseBlockPage();
        }
    });
}

function SaveMultipleCropedImage(saveFilePath, hdnImageName, postUrl) {
    StartBlockPage();
    var $image = $("#cropimage_" + hdnImageName);
    var result = $image.cropper("getCroppedCanvas");
    var imgData = result.toDataURL('image/png');
    var extension = $("#fileImage_" + hdnImageName).val().split('.').pop();

    var model = {
        imgBase64String: imgData,
        saveFilePath: saveFilePath,
        extension: extension
    };

    $.ajax({
        type: 'POST',
        url: postUrl,
        data: JSON.stringify(model),
        contentType: 'application/json; charset=utf-8',
        dataType: 'JSON',
        success: function (resultResponse) {
            $("#solid-alerts").show();
            $('#solid-alerts #message').text(resultResponse.Message);
            if (resultResponse.Status == 1) {
                $("#solid-alerts .alert").addClass("bg-primary");

                var div = '<div class="col-lg-3 col-md-6 col-xs-12 margintop20">';
                div += '<img class="img-thumbnail img-fluid img-custom-size" data-new="1" data-imagename="' + resultResponse.Result + '" src="' + imgData + '"/><br/><br/>';
                div += '<input type="radio" id="DefaultImage" name="DefaultImage" value="' + resultResponse.Result + '">Default</input><br/><br/>';
                div += '<a href="javascript:;" class="btn btn-danger removeImage">Remove</a>';
                div += '</div>';

                $('#divMultiUpload').append(div);
                $('#spanErrMultiUpload').hide();
            }
            else {
                $("#solid-alerts .alert").addClass("bg-warning");
                toastr.error(resultResponse.Message, 'Error !');
            }
            $("#Modal_" + hdnImageName).modal('hide');
            CloseBlockPage();
        },
        error: function (e) {
            $("#solid-alerts .alert").addClass("bg-danger");
            $("#solid-alerts").show();
            $('#solid-alerts #message').text(e.status + "," + e.statusText);
            CloseBlockPage();
        }
    });
}