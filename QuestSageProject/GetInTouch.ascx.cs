﻿using Newtonsoft.Json.Linq;
using QuestSageProject.DAL;
using System;
using System.IO;
using System.Net;
using System.Web.Services;
using System.Web.UI;

namespace QuestSageProject
{
    public partial class GetInTouch : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmitGetInTouch_Click(object sender, EventArgs e)
        {
            try
            {
                QuestsageEntities _ctx = new QuestsageEntities();

                DAL.GetInTouch data = new DAL.GetInTouch
                {
                    Name = txtName.Text,
                    Email = txtEmail.Text,
                    subject = txtSubject.Text,
                    Message = txtMessage.Text,
                    ContactNumber = txtContact.Text,
                    Budget = ddlBudget.SelectedValue
                };

                _ctx.GetInTouch.Add(data);
                _ctx.SaveChanges();

                txtEmail.Text = txtName.Text = txtSubject.Text = txtMessage.Text = txtContact.Text = string.Empty;

                Page.ClientScript.RegisterStartupScript(this.GetType(), "Success", "MessageSentSuccessfully()", true);
            }
            catch (Exception)
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Failed", "FailedToSendMessage()", true);
            }
        }

        [WebMethod]
        public static bool IsReCaptchValid(string captchaResponse)
        {
            string secretKey = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SecretKey");
            var result = false;
            var apiUrl = "https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}";
            var requestUri = string.Format(apiUrl, secretKey, captchaResponse);
            var request = (HttpWebRequest)WebRequest.Create(requestUri);

            using (WebResponse response = request.GetResponse())
            {
                using (StreamReader stream = new StreamReader(response.GetResponseStream()))
                {
                    JObject jResponse = JObject.Parse(stream.ReadToEnd());
                    var isSuccess = jResponse.Value<bool>("success");
                    result = (isSuccess) ? true : false;
                }
            }
            return result;
        }
    }
}