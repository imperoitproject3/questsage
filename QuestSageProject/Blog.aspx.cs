﻿using QuestSageProject.DAL;
using QuestSageProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuestSageProject
{
    public partial class Blog : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                int CategoryId = 0;
                if (Request.QueryString["CategoryId"] != null)
                {
                    hdnCategoryId.Value = Request.QueryString["CategoryId"];
                    CategoryId = Convert.ToInt32(hdnCategoryId.Value);
                }
                BindCategories();
                BindLateBlogs(CategoryId);
            }
        }

        private void BindLateBlogs(int CategoryId)
        {
            QuestsageEntities _ctx = new QuestsageEntities();
            List<BlogMasterModel> objBlogs = new List<BlogMasterModel>();

            if (CategoryId > 0)
            {
                objBlogs = (from b in _ctx.BlogMaster
                            where b.CategoryId == CategoryId
                            select new BlogMasterModel
                            {
                                BlogId = b.BlogId,
                                Title = b.Title,
                                Author = b.Author,
                                ImageUrl = b.ImageUrl,
                                PublishDate = b.PublishDate
                            }).OrderByDescending(x => x.PublishDate).ToList();
            }
            else
            {
                objBlogs = (from b in _ctx.BlogMaster
                            select new BlogMasterModel
                            {
                                BlogId = b.BlogId,
                                Title = b.Title,
                                Author = b.Author,
                                ImageUrl = b.ImageUrl,
                                PublishDate = b.PublishDate
                            }).OrderByDescending(x => x.PublishDate).ToList();
            }

            if (objBlogs != null && objBlogs.Any())
            {
                rptLatestBlog.DataSource = objBlogs;
                ltlLatestBlogTitle.Text = objBlogs.First().Title;
            }
            else
                rptLatestBlog.DataSource = null;
            rptLatestBlog.DataBind();
        }

        public static string ToTitle(string value)
        {
            return value.Replace(" ", "-").ToLower();
        }

        private void BindCategories()
        {
            QuestsageEntities _ctx = new QuestsageEntities();

            List<CategoryModel> lstCategories = (from c in _ctx.Category
                                                 select new CategoryModel
                                                 {
                                                     Id = c.Id,
                                                     Name = c.Name,
                                                     BlogCount = c.BlogMaster.Count()
                                                 }).Where(x => x.BlogCount > 0).ToList();

            rptCategories.DataSource = lstCategories;
            rptCategories.DataBind();
        }
    }
}