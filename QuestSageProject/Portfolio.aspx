﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewMaster.master" AutoEventWireup="true" CodeBehind="portfolio.aspx.cs" Inherits="QuestSageProject.Portfolio" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="title-bg-portfolio">
        <div class="container">
            <div class="page-title text-white text-center">
                <h1 data-animate="fadeInUp" data-delay="1.2">Our Brilliant Work</h1>
               <%-- <h2 data-animate="fadeInUp" data-delay="1.4">Increasing performance, productivity, scalability, and efficiency of your processes by using cost-effective industry leading frameworks is what we aim at.r</h2>--%>
            </div>
        </div>
    </section>
    <section class="pt-70 pb-70">
        <div class="container">
          <%--  <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-8">
                    <div class="section-title text-center">
                        <h2 data-animate="fadeInUp" data-delay=".1">Our Brilliant Work</h2>
                        <p data-animate="fadeInUp" data-delay=".3">Increasing performance, productivity, scalability, and efficiency of your processes by using cost-effective industry leading frameworks is what we aim at.</p>
                    </div>
                </div>
            </div>--%>
            <div class="row justify-content-center">
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="single-member" data-animate="fadeInUp" data-delay=".3">
                        <img src="img/Work/cooloff.jpg" alt="">
                        <div class="member-info bg-dark bg-rotate">
                            <h4>CoolOff</h4>
                            <span>An app that notifies you to start using greener methods when it's cooler outside.</span>
                            <ul class="list-unstyled text-right">
                                <li><a href="https://itunes.apple.com/us/app/cool-off-app/id1119480248?ls=1&mt=8" target="_blank"><i class="fab fa-apple"></i></a></li>
                                <li><a href="https://play.google.com/store/apps/details?id=com.cooloff.app" target="_blank"><i class="fab fa-android"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="single-member" data-animate="fadeInUp" data-delay=".3">
                        <img src="img/Work/audilex.jpg" alt="">
                        <div class="member-info bg-dark bg-rotate">
                            <h4>Audilex</h4>
                            <span>A great list of precedents for students who are pursuing their education in the law field.</span>
                            <ul class="list-unstyled text-right">
                                <li><a href="https://itunes.apple.com/us/podcast/audilex/id1209753970" target="_blank"><i class="fab fa-apple"></i></a></li>
                                <li><a href="https://play.google.com/store/apps/details?id=com.audilex&hl=en_GB" target="_blank"><i class="fab fa-android"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="single-member" data-animate="fadeInUp" data-delay=".3">
                        <img src="img/Work/clubman.jpg" alt="">
                        <div class="member-info bg-dark bg-rotate">
                            <h4>Clubman</h4>
                            <span>Tool to manage score and social media updates for sport games.</span>
                            <ul class="list-unstyled text-right">
                                <li><a href="https://itunes.apple.com/us/app/clubman/id1233698692?mt=8" target="_blank"><i class="fab fa-apple"></i></a></li>
                                <li><a href="https://play.google.com/store/apps/details?id=com.clubmanlive" target="_blank"><i class="fab fa-android"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                 <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="single-member" data-animate="fadeInUp" data-delay=".3">
                        <img src="img/Work/siente.jpg" alt="">
                        <div class="member-info bg-dark bg-rotate">
                            <h4>Siente</h4>
                            <span>Premium App to help user meditate and keep track of mental fitness</span>
                            <ul class="list-unstyled text-right">
                                <li><a href="https://itunes.apple.com/es/app/siente-mindfulness/id1135427078?mt=8" target="_blank"><i class="fab fa-apple"></i></a></li>
                                <li><a href="https://play.google.com/store/apps/details?id=com.facilisimo.dotmind" target="_blank"><i class="fab fa-android"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                 <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="single-member" data-animate="fadeInUp" data-delay=".3">
                        <img src="img/Work/peoplize.jpg" alt="">
                        <div class="member-info bg-dark bg-rotate">
                            <h4>Peoplize</h4>
                            <span>An app to create and host events. In addition, you can chat with the invited users as well.</span>
                            <ul class="list-unstyled text-right">
                                <li><a href="https://itunes.apple.com/gb/app/peoplize-meet-new-people/id1314899645?mt=8" target="_blank"><i class="fab fa-apple"></i></a></li>
                                <li><a href="https://play.google.com/store/apps/details?id=com.peoplize&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1" target="_blank"><i class="fab fa-android"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="single-member" data-animate="fadeInUp" data-delay=".3">
                        <img src="img/Work/jiffyrides.jpg" alt="">
                        <div class="member-info bg-dark bg-rotate">
                            <h4>Jiffy Rides</h4>
                            <span>App to help car pool users with same work commute patterns</span>
                            <ul class="list-unstyled text-right">
                                <li><a href="#" target="_blank"><i class="fab fa-apple"></i></a></li>
                                <li><a href="#" target="_blank"><i class="fab fa-android"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="single-member" data-animate="fadeInUp" data-delay=".3">
                        <img src="img/Work/rewardboard.jpg" alt="">
                        <div class="member-info bg-dark bg-rotate">
                            <h4>Deeds 4 Kids</h4>
                            <span>App to help parents keep track and reward child for good deeds.</span>
                            <ul class="list-unstyled text-right">
                                <li><a href="https://itunes.apple.com/us/app/rewardboard/id1084960289?mt=8" target="_blank"><i class="fab fa-apple"></i></a></li>
                                <li><a href="https://play.google.com/store/apps/details?id=com.impero.rewardboard&hl=en" target="_blank"><i class="fab fa-android"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="single-member" data-animate="fadeInUp" data-delay=".3">
                        <img src="img/Work/hurler.jpg" alt="">
                        <div class="member-info bg-dark bg-rotate">
                            <h4>The Hurler</h4>
                            <span>App for managing and keeping track of sport personell</span>
                            <ul class="list-unstyled text-right">
                                <li><a href="https://itunes.apple.com/gb/app/gaa-player-development-the-hurler/id1241636178?mt=8" target="_blank"><i class="fab fa-apple"></i></a></li>
                                <li><a href="https://play.google.com/store/apps/details?id=com.thehurler&hl=en" target="_blank"><i class="fab fa-android"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
</asp:Content>

