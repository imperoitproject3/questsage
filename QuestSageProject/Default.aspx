﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewMaster.master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="QuestSageProject.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Mobile App Development Services India -Quest Sage</title>
    <meta name="description" content="QuestSage IT Services, is leading software company which provide top custom mobile app development services in India," />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="main-banner bg-primary bg-rotate position-relative">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-4">
                    <div class="banner-image text-center" data-animate="fadeInUp" data-delay="1.2">
                        <img src="<%= Page.ResolveUrl("~/img/cloud.png") %>" alt="">
                    </div>
                </div>
                <div class="col-lg-6 col-md-8">
                    <div class="banner-content text-white">
                        <h2 data-animate="fadeInUp" data-delay="1.4">Top Mobile App Developers India</h2>
                        <p data-animate="fadeInUp" data-delay="1.5">QuestSage IT Services is a comprehensive mobile app development services company, which varied mobile app development services in India for several domains. </p>
                        <ul class="list-inline" data-animate="fadeInUp" data-delay="1.6">
                            <li><a class="btn btn-transparent" href="portfolio.aspx">View Portfolio</a></li>
                            <li><a class="btn btn-transparent" href="contact.aspx">Free Quote</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <span class="goDown"><i class="fas fa-arrow-down bounce"></i></span>
        </div>
    </section>
    <section class="home-features pt-175 pb-120">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-sm-6">
                    <div class="single-home-feature" data-animate="fadeInUp" data-delay=".1">
                        <img class="svg" src="img/skills/mobile.png" alt="">
                        <h3>Mobile App Developement</h3>
                        <p>Experience user-centric mobile apps – your idea, our implementation</p>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single-home-feature" data-animate="fadeInUp" data-delay=".3">
                        <img class="svg" src="img/skills/game.png" alt="">
                        <h3>Game Development</h3>
                        <p>Encounter convenient, value-based games – experience fun, progress, and accomplishment</p>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single-home-feature" data-animate="fadeInUp" data-delay=".5">
                        <img class="svg" src="img/skills/blockchain.png" alt="">
                        <h3>Blockchain Development</h3>
                        <p>Settle securities and transfer power from core to periphery of networks</p>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single-home-feature" data-animate="fadeInUp" data-delay=".7">
                        <img class="svg" src="img/skills/cross_platform.png" alt="">
                        <h3>Cross-platform Development</h3>
                        <p>Discover flexible visage, feel, function, and performance across multi-platforms and devices</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="why-us pb-70">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="section-title text-left">
                        <h2 data-animate="fadeInUp" data-delay=".1">Why opt for QuestSage IT Services?</h2>
                        <p data-animate="fadeInUp" data-delay=".2">
                            QuestSage IT Services is one dynamic firm that is compact in size and boundless in ambition.<br />
                            We do not just mean to expand our app development services in India but also scale to clients abroad, focusing on technological solutions to each of our client’s problems.<br />
                            <br />
                            Here’s why you should opt for QuestSage IT Services for getting app developers for hire:
                        </p>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="single-reason" data-animate="fadeInUp" data-delay=".3">
                                <h3>01. Attention to Relationship</h3>
                                <p>For us, performance is important but being transparent, and preserving relationships is more important. Our attachment to future technologies and our distinctive traits that have helped in a transformation of enterprises are based on a human-centered process.</p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="single-reason" data-animate="fadeInUp" data-delay=".5">
                                <h3>02. Exclusively Focused</h3>
                                <p>With most service providers, it is evident that your needs would be lost in a haystack, with divided attention and lack of enough problem-solving. Replete Software with its handful of experts and focused clientele delivers increased accountability and enhanced decision-making.</p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="single-reason" data-animate="fadeInUp" data-delay=".6">
                                <h3>03. Shrinking IT investments</h3>
                                <p>We repose our firm belief in our client’s IT budget that it would shrink by many levels in coming years. By implementing Proactive Problem Management, we hope to cut down on our client expenses in the IT infrastructure space.</p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="single-reason" data-animate="fadeInUp" data-delay=".5">
                                <h3>04. Engagement Approach</h3>
                                <p>Unlike traditional IT players who use fresh recruits for manual tasks, we assign our expert consultants with an individual client account, strengthening the bond and promoting stability and client retention. </p>
                            </div>
                        </div>
                    </div>
                    <a href="contact.aspx" class="btn btn-primary" data-animate="fadeInUp" data-delay=".7">Let's Talk Ideas</a>
                </div>
                <div class="col-lg-5">
                    <div class="why-us-video why-us-video-sticky-tape text-center text-white position-relative" data-animate="fadeInUp" data-delay=".1">
                        <div class="sticky-tape">
                            <img src="img/Home-video-bg.jpg" alt="">
                            <p>
                                <a class="pl-video" data-toggle="modal" data-target="#video-popup">
                                    <img src="img/play.png" alt="">
                                </a><%--<span>Intro: QuestSage IT Services</span>--%>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="pt-70 pb-70">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-8">
                    <div class="section-title text-center">
                        <h2 data-animate="fadeInUp" data-delay=".1">Our Brilliant Work</h2>
                      <%--  <p data-animate="fadeInUp" data-delay=".3">Increasing performance, productivity, scalability, and efficiency of your processes by using cost-effective industry leading frameworks is what we aim at.</p>--%>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="single-member" data-animate="fadeInUp" data-delay=".3">
                        <img src="img/Work/cooloff.jpg" alt="">
                        <div class="member-info bg-dark bg-rotate">
                            <h4>CoolOff</h4>
                            <span>An app that notifies you to start using greener methods when it's cooler outside.</span>
                            <ul class="list-unstyled text-right">
                                <li><a href="https://itunes.apple.com/us/app/cool-off-app/id1119480248?ls=1&mt=8" target="_blank"><i class="fab fa-apple"></i></a></li>
                                <li><a href="https://play.google.com/store/apps/details?id=com.cooloff.app" target="_blank"><i class="fab fa-android"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="single-member" data-animate="fadeInUp" data-delay=".3">
                        <img src="img/Work/audilex.jpg" alt="">
                        <div class="member-info bg-dark bg-rotate">
                            <h4>Audilex</h4>
                            <span>A great list of precedents for students who are pursuing their education in the law field.</span>
                            <ul class="list-unstyled text-right">
                                <li><a href="https://itunes.apple.com/us/podcast/audilex/id1209753970" target="_blank"><i class="fab fa-apple"></i></a></li>
                                <li><a href="https://play.google.com/store/apps/details?id=com.audilex&hl=en_GB" target="_blank"><i class="fab fa-android"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="single-member" data-animate="fadeInUp" data-delay=".3">
                        <img src="img/Work/clubman.jpg" alt="">
                        <div class="member-info bg-dark bg-rotate">
                            <h4>Clubman</h4>
                            <span>Tool to manage score and social media updates for sport games.</span>
                            <ul class="list-unstyled text-right">
                                <li><a href="https://itunes.apple.com/us/app/clubman/id1233698692?mt=8" target="_blank"><i class="fab fa-apple"></i></a></li>
                                <li><a href="https://play.google.com/store/apps/details?id=com.clubmanlive" target="_blank"><i class="fab fa-android"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="single-member" data-animate="fadeInUp" data-delay=".3">
                        <img src="img/Work/siente.jpg" alt="">
                        <div class="member-info bg-dark bg-rotate">
                            <h4>Siente</h4>
                            <span>Premium App to help user meditate and keep track of mental fitness</span>
                            <ul class="list-unstyled text-right">
                                <li><a href="https://itunes.apple.com/es/app/siente-mindfulness/id1135427078?mt=8" target="_blank"><i class="fab fa-apple"></i></a></li>
                                <li><a href="https://play.google.com/store/apps/details?id=com.facilisimo.dotmind" target="_blank"><i class="fab fa-android"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="single-member" data-animate="fadeInUp" data-delay=".3">
                        <img src="img/Work/peoplize.jpg" alt="">
                        <div class="member-info bg-dark bg-rotate">
                            <h4>Peoplize</h4>
                            <span>An app to create and host events. In addition, you can chat with the invited users as well.</span>
                            <ul class="list-unstyled text-right">
                                <li><a href="https://itunes.apple.com/gb/app/peoplize-meet-new-people/id1314899645?mt=8" target="_blank"><i class="fab fa-apple"></i></a></li>
                                <li><a href="https://play.google.com/store/apps/details?id=com.peoplize&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1" target="_blank"><i class="fab fa-android"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="single-member" data-animate="fadeInUp" data-delay=".3">
                        <img src="img/Work/jiffyrides.jpg" alt="">
                        <div class="member-info bg-dark bg-rotate">
                            <h4>Jiffy Rides</h4>
                            <span>App to help car pool users with same work commute patterns.</span>
                            <ul class="list-unstyled text-right">
                                <li><a href="https://itunes.apple.com/us/app/jiffy-rides/id970968243?mt=8" target="_blank"><i class="fab fa-apple"></i></a></li>
                                <li><a href="https://play.google.com/store/apps/details?id=com.jiffyrides.android&hl=en" target="_blank"><i class="fab fa-android"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="single-member" data-animate="fadeInUp" data-delay=".3">
                        <img src="img/Work/rewardboard.jpg" alt="">
                        <div class="member-info bg-dark bg-rotate">
                            <h4>Deeds 4 Kids</h4>
                            <span>App to help parents keep track and reward child for good deeds.</span>
                            <ul class="list-unstyled text-right">
                                <li><a href="https://itunes.apple.com/us/app/rewardboard/id1084960289?mt=8" target="_blank"><i class="fab fa-apple"></i></a></li>
                                <li><a href="https://play.google.com/store/apps/details?id=com.impero.rewardboard&hl=en" target="_blank"><i class="fab fa-android"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="single-member" data-animate="fadeInUp" data-delay=".3">
                        <img src="img/Work/hurler.jpg" alt="">
                        <div class="member-info bg-dark bg-rotate">
                            <h4>The Hurler</h4>
                            <span>App for managing and keeping track of sport personell</span>
                            <ul class="list-unstyled text-right">
                                <li><a href="https://itunes.apple.com/gb/app/gaa-player-development-the-hurler/id1241636178?mt=8" target="_blank"><i class="fab fa-apple"></i></a></li>
                                <li><a href="https://play.google.com/store/apps/details?id=com.thehurler&hl=en" target="_blank"><i class="fab fa-android"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="reviews bg-light bg-rotate position-relative">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-8">
                    <div class="section-title text-center">
                        <h2 data-animate="fadeInUp" data-delay=".1">Our Client’s Words</h2>
                       <%-- <p data-animate="fadeInUp" data-delay=".2">Client satisfaction is our goal and we value our patronage. Here is what our clients say.</p>--%>
                    </div>
                </div>
            </div>
            <div class="swiper-container review-slider">
                <div class="swiper-wrapper">
                    <div class="swiper-slide bg-dark bg-rotate single-review">
                        <div class="review-info">
                            <i class="fas fa-quote-left float-left"></i>
                            <h4>Rufus Owen</h4>
                            <span>USA</span>
                        </div>
                        <p>"Very hard-working team. We are highly impressed by the way they beat challenges and navigated our issues, thereby helping in shaping a successful product. They actually worked much faster and more efficiently than we expected. We would strongly recommend this firm to anyone who is on a lookout for quality, time-efficient, and frustration-free development process.</p>
                    </div>
                    <div class="swiper-slide bg-dark bg-rotate single-review">
                        <div class="review-info">
                            <i class="fas fa-quote-left float-left"></i>
                            <h4>Amabel  Aimée</h4>
                            <span>Uk</span>
                        </div>
                        <p>"The technical acumen and professionalism exhibited by Quest Sage helps in building a strong and positive relationship with them. Our team thoroughly enjoyed working with them as there were no communication gaps throughout the process. They built trust implicitly as they were always ready to go the extra mile. We look forward to engage them again for our future projects.</p>
                    </div>
                    <div class="swiper-slide bg-dark bg-rotate single-review">
                        <div class="review-info">
                            <i class="fas fa-quote-left float-left"></i>
                            <h4>Wilder Shepherd</h4>
                            <span>Canada</span>
                        </div>
                        <p>"The way they made our idea take shape and turn into an app using graphics, animation, and content revealed their innovative abilities and creative mindset. The team at Quest Sage not only has a technical expertise to surmount challenges but also creative outlook to enchant the clients. I could not be happier with my decision to choose Quest Sage for my project.</p>
                    </div>
                    <div class="swiper-slide bg-dark bg-rotate single-review">
                        <div class="review-info">
                            <i class="fas fa-quote-left float-left"></i>
                            <h4>Karim Zyaire</h4>
                            <span>Dubai</span>
                        </div>
                        <p>"Great efforts were put in by the Quest Sage team in enlivening our challenging app idea. I really admire the hard work, professionalism, and punctuality exhibited by the team in providing us with the final outcome. I highly recommend Quest Sage and myself look forward to work with them again.</p>
                    </div>
                    <div class="swiper-slide bg-dark bg-rotate single-review">
                        <div class="review-info">
                            <i class="fas fa-quote-left float-left"></i>
                            <h4>Percy Silas</h4>
                            <span>Ireland</span>
                        </div>
                        <p>"I am so glad that I contacted Quest Sage for my project on the recommendation of a colleague. From the very first conversation with them I knew that they value their clients. The way they understood our needs was very impressive. Thumbs up to the team for their strategic and professional rigour combined with far-sighted imagination.</p>
                    </div>
                </div>
            </div>
            <div class="swiper-pagination review-pagination"></div>
        </div>
    </section>
    <section class="latest-news pt-175 pb-120">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-8">
                    <div class="section-title text-center">
                        <h2 data-animate="fadeInUp" data-delay=".1">Latest Blogs</h2>
                        <p data-animate="fadeInUp" data-delay=".2">Strop Your Technical Knowledge with Us.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <asp:Repeater ID="rptBlogs" runat="server">
                    <ItemTemplate>
                        <div class="col-md-4">
                            <div class="single-post" data-animate="fadeInUp" data-delay=".1">
                                <a href="#">
                                    <img src="<%# Page.ResolveUrl("~/Files/BlogImages/"+Eval("ImageUrl")) %>" alt="">
                                </a>
                                <div class="post-content">
                                    <p class="post-info">
                                        Posted on <a href="javascript:;"><%# string.Format("{0:MMMM dd, yyyy}", Eval("PublishDate")) %></a> by <%# Eval("Author") %>
                                    </p>
                                    <h3><a href="<%# Page.GetRouteUrl("Blog",new { title= ToTitle(Eval("Title").ToString()), id=Eval("BlogId") }) %>"><%# Eval("Title") %></a></h3>
                                    <p><%# Eval("BlogBody") %></p>
                                    <a href="#"></a>
                                    <br />
                                    <a href="<%# Page.GetRouteUrl("Blog",new { title= ToTitle(Eval("Title").ToString()), id=Eval("BlogId") }) %>">Reading Continue <i class="fas fa-caret-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <ul class="our-clients list-unstyled d-md-flex justify-content-md-between">
                <li data-animate="fadeInUp" data-delay=".1">
                    <img src="img/Client/Daxium.png" alt=""></li>
                <li data-animate="fadeInUp" data-delay=".2">
                    <img src="img/Client/Jawed_habib.png" alt=""></li>
                <li data-animate="fadeInUp" data-delay=".3">
                    <img src="img/Client/Kohler.png" alt=""></li>
                <li data-animate="fadeInUp" data-delay=".4">
                    <img src="img/Client/Mitsumi.png" alt=""></li>
                <li data-animate="fadeInUp" data-delay=".5">
                    <img src="img/Client/Remax.png" alt=""></li>
            </ul>
        </div>
    </section>
    <div class="modal fade" id="video-popup" tabindex="-1" role="dialog" aria-labelledby="videoLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <p>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </p>
                    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/1B4qqQkjb7A?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

