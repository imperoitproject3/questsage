﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Contact.ascx.cs" Inherits="QuestSageProject.Contact" %>

<script src="Content/Plugins/Validate/jquery.unobtrusive-ajax.min.js"></script>
<div>
    <div class="row half-gutters mb-0">
        <div class="col-md-4">
            <asp:TextBox ID="txtName" runat="server" CssClass="form-control" placeholder="Name*" data-animate="fadeInUp" data-delay=".3"></asp:TextBox>
            <asp:RequiredFieldValidator Style="color: red;" runat="server" ControlToValidate="txtName" ErrorMessage="Please enter name" ValidationGroup="Inquiry" Display="Dynamic"></asp:RequiredFieldValidator>
            <%--<input type="text" name="name" class="form-control" placeholder="Name*" required data-animate="fadeInUp" data-delay=".3">--%>
        </div>
        <div class="col-md-4">
            <asp:TextBox ID="txtCountry" runat="server" CssClass="form-control" placeholder="Country*" data-animate="fadeInUp" data-delay=".3"></asp:TextBox>
            <asp:RequiredFieldValidator Style="color: red;" runat="server" ControlToValidate="txtCountry" ErrorMessage="Please enter country" ValidationGroup="Inquiry" Display="Dynamic"></asp:RequiredFieldValidator>
        </div>
        <div class="col-md-4">
            <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" placeholder="E-mail*" data-animate="fadeInUp" data-delay=".4"></asp:TextBox>
            <asp:RequiredFieldValidator Style="color: red;" runat="server" ControlToValidate="txtEmail" ErrorMessage="Please enter email" ValidationGroup="Inquiry" Display="Dynamic"></asp:RequiredFieldValidator>
            <br />
            <asp:RegularExpressionValidator Style="color: red;" ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtEmail" ErrorMessage="Please enter valid email" ValidationGroup="Inquiry"></asp:RegularExpressionValidator>

            <%--<input type="email" name="email" class="form-control" placeholder="E-mail*" required data-animate="fadeInUp" data-delay=".4">--%>
        </div>
        <div class="col-md-4">
            <asp:TextBox ID="txtContact" runat="server" CssClass="form-control" placeholder="Contact Number*" data-animate="fadeInUp" data-delay=".4"></asp:TextBox>
            <asp:RequiredFieldValidator Style="color: red;" runat="server" ControlToValidate="txtContact" ErrorMessage="Please enter contact" ValidationGroup="Inquiry" Display="Dynamic"></asp:RequiredFieldValidator>

            <%--<input type="email" name="email" class="form-control" placeholder="Contact Number*" required data-animate="fadeInUp" data-delay=".4">--%>
        </div>
        <div class="col-md-4">
            <asp:TextBox ID="txtSkype" runat="server" CssClass="form-control" placeholder="Skype" data-animate="fadeInUp" data-delay=".5"></asp:TextBox>

            <%--<input type="text" name="Skype" class="form-control" placeholder="Skype" data-animate="fadeInUp" data-delay=".5">--%>
        </div>
        <div class="col-md-4">
            <asp:DropDownList ID="ddlBudget" runat="server">
                <asp:ListItem Text="Budget (US $)" Value=""></asp:ListItem>
                <asp:ListItem Text="Not Fixed" Value="Not Fixed"></asp:ListItem>
                <asp:ListItem Text="Below 5k" Value="Below 5k"></asp:ListItem>
                <asp:ListItem Text="5k-10k" Value="5k-10k"></asp:ListItem>
                <asp:ListItem Text="10k-20k" Value="10k-20k"></asp:ListItem>
                <asp:ListItem Text="20k-50k" Value="20k-50k"></asp:ListItem>
                <asp:ListItem Text="Above 50k" Value="Above 50k"></asp:ListItem>
            </asp:DropDownList>
        </div>

        <div class="col-md-12">
            <label class="checkboxcontainer">
                NDA Required?
            <input type="checkbox" id="chkNDARequired" runat="server">
                <span class="checkmark"></span>
            </label>

        </div>

        <div class="col-md-12">
            Upload Project Docs<br />
            <asp:FileUpload ID="fileInquiry" ClientIDMode="Static" runat="server" />
        </div>
    </div>
    <div class="position-relative">
        <asp:TextBox ID="txtProjectDetail" TextMode="MultiLine" runat="server" CssClass="form-control" placeholder="Project Details*" data-animate="fadeInUp" data-delay=".2"></asp:TextBox>

        <asp:RequiredFieldValidator Style="color: red;" runat="server" ControlToValidate="txtProjectDetail" ErrorMessage="Please enter project detail" ValidationGroup="Inquiry" Display="Dynamic" ></asp:RequiredFieldValidator>
        <%--<textarea name="text" class="form-control" placeholder="Project Details*" required data-animate="fadeInUp" data-delay=".2"></textarea>--%>
    </div>
    <div class="col-md-12">
        <div id="ReCaptchContainer"></div>
        <label id="lblMessage" runat="server" clientidmode="static" style="display: contents;"></label>
    </div>
    <asp:Button ID="btnSubmitInquiry" Style="color: white !important" runat="server" data-animate="fadeInUp" data-delay=".6" CssClass="btn btn-primary test" Text="Submit Inquiry" OnClick="btnSubmitInquiry_Click" ValidationGroup="Inquiry" />
    <%--    <button id="btnSubmitInquiry" class="btn btn-primary" data-animate="fadeInUp" data-delay=".6">Submit Inquiry</button>--%>
</div>
<script type="text/javascript">  
    var istrue = false;
    $('#form1').submit(function () {
        var data = { 'captchaResponse': $('#g-recaptcha-response').val() };
        $.ajax({
            type: "POST",
            url: "contact.aspx/IsReCaptchValid",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify(data),
            success: function (data) {
                var returnedstring = data.d;
                if (returnedstring == false) {
                    jQuery('#lblMessage').css('color', 'red').html('Please check the Recaptcha');
                }
                else {
                    jQuery('#lblMessage').css('color', 'green').html(' ');
                    istrue = true;
                }
            },
            failure: function (errMsg) {
                alert(errMsg);
            },
            error: (error) => {
                console.log(JSON.stringify(error));
            }
        });
        console.log(istrue);
        return istrue;
    });

    var your_site_key = '<%= ConfigurationManager.AppSettings["SiteKey"]%>';
    var renderRecaptcha = function () {
        grecaptcha.render('ReCaptchContainer', {
            'sitekey': your_site_key,
            'callback': reCaptchaCallback,
            theme: 'light', //light or dark    
            type: 'image',// image or audio    
            size: 'normal'//normal or compact    
        });
    };

    var reCaptchaCallback = function (response) {
        if (response !== '') {
            //jQuery('#lblMessage').css('color', 'green').html('Success');
            istrue = true;
        }
    };

    jQuery('button[type="button"]').click(function (e) {
        var message = 'Please check the Recaptcha';
        if (typeof (grecaptcha) != 'undefined') {
            var response = grecaptcha.getResponse();
            (response.length === 0) ? (message = 'Captcha verification failed') : (message = ' ');
        }
        jQuery('#lblMessage').html(message);
        jQuery('#lblMessage').css('color', (message.toLowerCase() == 'success!') ? "green" : "red");
    });

</script>
<script type="text/javascript">
    $('#fileInquiry').on('change', function () {
        var val = $(this).val().toLowerCase(), regex = new RegExp("(.*?)\.(docx|doc|pdf|xml|bmp|ppt|xls)$");
        if (!(regex.test(val))) {
            $(this).val('');
            alert('Please select document file only');
            return false;
        }
    });
    function InquirySubmittedSuccessfully() {
        alert('Inquiry submitted successfully!');
    }
    function FailedToSendInquiry() {
        alert('Failed to send Inquiry, please try again later');
    }
</script>
