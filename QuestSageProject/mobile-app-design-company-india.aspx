﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewMaster.master" AutoEventWireup="true" CodeBehind="mobile-app-design-company-india.aspx.cs" Inherits="QuestSageProject.mobile_app_design_company_india" %>

<%@ Register Src="~/Contact.ascx" TagPrefix="uc1" TagName="ContactUC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Mobile Game Developers India, Mobile Game Development Company USA</title>
 <meta name="description" content="QuestSage IT Services is a leading Mobile game development company in India offering expert team of Build box, Unity3D, AR and VR mobile game developers."/>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="title-bg-appdesign">
        <div class="container">
            <div class="page-title text-white text-center">
                <h1 data-animate="fadeInUp" data-delay="1.2">Mobile app design Company India </h1>
                <%--<h2 data-animate="fadeInUp" data-delay="1.4">Some Tagline here</h2>--%>
            </div>
        </div>
    </section>
    <section class="pt-70">
        <div class="container">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-sm-6">
                        <div class="single-home-feature" data-animate="fadeInUp" data-delay=".1">
                            <img class="svg" src="img/high.svg" alt="">
                            <h3>Discovery
                            </h3>
                            <p>
                                As a niche mobile app design company, we dive into the ideas deeper to understand the objectives, brand and the target audience. Our team is super enthusiastic while brewing the fresh ideas and curate to something new, every time. 
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="single-home-feature" data-animate="fadeInUp" data-delay=".3">
                            <img class="svg" src="img/security.svg" alt="">
                            <h3>UI/UX Design

                            </h3>
                            <p>Our focus lies in designing interactive and powerful UI/UX. We focus in detail on the responsive web design and the cross-platform apps. </p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="single-home-feature" data-animate="fadeInUp" data-delay=".5">
                            <img class="svg" src="img/guard.svg" alt="">
                            <h3>Prototyping
                            </h3>
                            <p>This is where the real work starts. It is all about connecting the screens, animations and the structures to give a lively feel to the app.</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="single-home-feature" data-animate="fadeInUp" data-delay=".7">
                            <img class="svg" src="img/support.svg" alt="">
                            <h3>Visual design
                            </h3>
                            <p>We thus design the apps focusing on the client’s needs and experiences. We add the final touch and branding and create a visual design. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="pt-70">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="single-post-page blog-left">
                        <div class="blog-details">
                            <div class="post-image">
                                <img src="img/services/appdesign.png" alt="" data-animate="fadeInUp" data-delay=".1">
                             <%--   <a href="contact.aspx">We focus on the ideas, design and the rich customer experience in everything we do. </a>--%>
                            </div>
                            <div class="post-detail-content">
                                <p data-animate="fadeInUp" data-delay=".2">
                                    We at QuestSage a mobile app development company believe in crafting the best mobile app design for our clientele. We are a leading Mobile App Design Company in India & USA that focuses on each and every pixel that entails a detailed design layout for mobile apps. Our team of mobile app designers aim at converting the ideas into pixels and into unique designs.
                                    <br />
                                    <br />
                                    Additionally, our excellent team of mobile app designers can tailor web design as per client’s brand and offerings with design solutions geared towards leading mobile app platforms - Android and iOS. 

                                </p>


                                <h3>Our Mobile App Design Approach - Design-first, build later

                                </h3>


                                <ul class="list-group">
                                    <li class="list-group-item">How will be the look and feel of the app? 
                                    </li>
                                    <li class="list-group-item">Will it be user-friendly and interactive? 
                                    </li>
                                    <li class="list-group-item">How to make the apps look more attractive?
                                    </li>

                                </ul>
                                <br />
                                <p>
                                    <b>Well, hire our mobile app designers can provide solutions and answers to all your questions. </b>
                                    <br />
                                    <br />
                                    We focus on the design-first approach. To us, the UX/UI design matters more than the actual content as it is the abstract nature that pulls the viewer in.
                                    <br />
                                    <br />
                                    We discuss, create and share the ideas of the best mobile app designs as per our client’s needs. 
Our designs enhance the great engineering and powerful UI at the same time. The design process we follow is interactive, collaborative and fun.
                                    <br />
                                    <br />
                                    We create the layouts with design mockups, wireframes, prototypes or animations. We later bring those ideas to life.
                                    <br />
                                    <br />
                                    We analyze the visual designs to transform what you “see” in terms of what you “feel”. This process helps us focus on the experience the mobile app platforms bring before we build the apps.   
                                    <br />

                                    Like a competent mobile app design company in India & USA, the intricacies of the pixels as well as the science of space matters to us. We develop robust UX design that saves both your time and money from the start until the end. Whether it is an Android app design or iPhone app design, we follow design strategies according to international standards and trends too!
                                </p>
                                <br />
                                <br />

                                <hr />
                                <div class="comment-form">
                                    <h3 class="widget-title" data-animate="fadeInUp" data-delay=".1">Contact us to know more about the expertise of our mobile app designers and our offerings, and also how we can serve your design requirements better!
                                    </h3>

                                    <uc1:ContactUC runat="server" ID="ContactUC" />

                                </div>
                                <section class="latest-news">
                                    <div class="container">
                                        <div class="row justify-content-center">
                                            <div class="col-xl-8 col-lg-8">
                                                <div class="section-title text-center">
                                                    <h2 data-animate="fadeInUp" data-delay=".1">Latest Designed Mobile Apps</h2>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 col-sm-6">
                                                <div class="single-member" data-animate="fadeInUp" data-delay=".3">
                                                    <img src="img/Work/hurler.jpg" alt="">
                                                    <div class="member-info bg-dark bg-rotate">
                                                        <h4>The Hurler</h4>
                                                        <span>App for managing and keeping track of sport personnel</span>
                                                        <ul class="list-unstyled text-right">
                                                            <li><a href="https://itunes.apple.com/gb/app/gaa-player-development-the-hurler/id1241636178?mt=8" target="_blank"><i class="fab fa-apple"></i></a></li>
                                                            <li><a href="https://play.google.com/store/apps/details?id=com.thehurler&hl=en" target="_blank"><i class="fab fa-android"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-sm-6">
                                                <div class="single-member" data-animate="fadeInUp" data-delay=".3">
                                                    <img src="img/Work/peoplize.jpg" alt="">
                                                    <div class="member-info bg-dark bg-rotate">
                                                        <h4>Peoplize</h4>
                                                        <span>An app to create and host events. You can also chat with the invited users as well.</span>
                                                        <ul class="list-unstyled text-right">
                                                            <li><a href="https://itunes.apple.com/gb/app/peoplize-meet-new-people/id1314899645?mt=8" target="_blank"><i class="fab fa-apple"></i></a></li>
                                                            <li><a href="https://play.google.com/store/apps/details?id=com.peoplize&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1" target="_blank"><i class="fab fa-android"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="sidebar-widget" data-animate="fadeInUp" data-delay=".1">
                        <h3 class="widget-title" data-animate="fadeInUp" data-delay=".2">Client Speaks!

                        </h3>

                        <div class="why-us-video text-center text-white position-relative" data-animate="fadeInUp" data-delay=".1">

                            <img src="img/Video/bob.png" alt="">
                            <p>
                                <a class="pl-video" data-toggle="modal" data-target="#video-popup">
                                    <img src="img/play.png" alt="">
                                </a><span></span>
                            </p>


                        </div>
                    </div>
                    <br />
                    <br />

                    <aside>
                        <div class="sidebar-widget" data-animate="fadeInUp" data-delay=".1">
                            <h3 class="widget-title" data-animate="fadeInUp" data-delay=".2">Tools and Technologies


                            </h3>


                            <ul class="list-group">

                                <li class="list-group-item">Adobe Photoshop

                                </li>
                                <li class="list-group-item">HTML5 and CSS3

                                </li>
                                <li class="list-group-item">JavaScript

                                </li>
                                <li class="list-group-item">Adobe Illustrator

                                </li>
                                <li class="list-group-item">Mockflow

                                </li>
                                <li class="list-group-item">CorelDraw
                                </li>
                                <li class="list-group-item">Adobe InDesign
                                </li>
                                <li class="list-group-item">Sketch 
                                </li>
                                <li class="list-group-item">Zeplin 
                                </li>
                                <li class="list-group-item">Marvel

                                </li>
                            </ul>

                        </div>
                        <div class="sidebar-widget" data-animate="fadeInUp" data-delay=".1">
                            <h3 class="widget-title" data-animate="fadeInUp" data-delay=".2">Benefits of our Mobile App Design Services 


                            </h3>
                            <ul class="sidebar-list list-unstyled">

                                <li data-animate="fadeInUp" data-delay=".3">Turn ideas into real apps with efficiency and effectiveness
                                </li>
                                <li data-animate="fadeInUp" data-delay=".3">Experienced team of mobile app designers well versed with multiple verticals, demands, and variables
                                </li>
                                <li data-animate="fadeInUp" data-delay=".4">Brilliant app design themes and ideas conforming to the platform’s design guidelines
                                </li>
                                <li data-animate="fadeInUp" data-delay=".5">Expert graphic designers that can blend UI according to the brand to reach intended new customers
                                </li>
                                <li data-animate="fadeInUp" data-delay=".6">Highly reasonable pricing and packages
                                </li>

                                <li data-animate="fadeInUp" data-delay=".7">Guaranteed compatibility and adherence to screen sizes and resolutions</li>



                            </ul>
                        </div>
                        <br />
                        <br />
                    </aside>
                </div>
            </div>
        </div>
        <div class="modal fade" id="video-popup" tabindex="-1" role="dialog" aria-labelledby="videoLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <p>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </p>
                        <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Wfj1W2htBIs?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

