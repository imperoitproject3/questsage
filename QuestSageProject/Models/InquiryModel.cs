﻿namespace QuestSageProject.Models
{
    public class InquiryModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public string Email { get; set; }
        public string ContactNumber { get; set; }
        public string Skype { get; set; }
        public string Budget { get; set; }
        public bool NDARequired { get; set; }
        public string DocumentFileName { get; set; }
        public string ProjectDetails { get; set; }
        public string Remarks { get; set; }
    }
}