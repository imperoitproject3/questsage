﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewMaster.master" AutoEventWireup="true" CodeBehind="blog.aspx.cs" Inherits="QuestSageProject.Blog" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .active {
            font-weight: bold;
        }
    </style>
    <script type="text/javascript">        
        $(function () {
            if ($('#hdnCategoryId').val()) {
                $('.category' + $('#hdnCategoryId').val()).addClass('active');
            }
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="title-bg-blog">
        <div class="container">
            <div class="page-title text-white text-center">
                <h1 data-animate="fadeInUp" data-delay="1.2">Latest Blog </h1>
                <span data-animate="fadeInUp" data-delay="1.4">
                    <asp:Literal ID="ltlLatestBlogTitle" runat="server"></asp:Literal>
                    <asp:HiddenField ID="hdnCategoryId" ClientIDMode="Static" runat="server" />
                </span>
            </div>
        </div>
    </section>
    <section class="pt-120">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <aside>

                        <div class="sidebar-widget" data-animate="fadeInUp" data-delay=".1">
                            <h3 class="widget-title" data-animate="fadeInUp" data-delay=".2">Categories</h3>

                            <ul class="sidebar-list karla list-unstyled">
                                <asp:Repeater ID="rptCategories" runat="server">
                                    <ItemTemplate>
                                        <li data-animate="fadeInUp" data-delay=".3" class="category<%# Eval("Id") %>">
                                            <a href="<%# Page.ResolveUrl("blog.aspx?CategoryId="+Eval("Id")) %>"><%# Eval("Name") %> <span>(<%# Eval("BlogCount") %>)</span></a>

                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>

                            <%--<ul class="sidebar-list karla list-unstyled">
                                <li data-animate="fadeInUp" data-delay=".3"><a href="#">Technology <span>(36)</span></a></li>
                                <li data-animate="fadeInUp" data-delay=".4"><a href="#">Business <span>(48)</span></a></li>
                                <li data-animate="fadeInUp" data-delay=".5"><a href="#">Cloud News <span>(83)</span></a></li>
                                <li data-animate="fadeInUp" data-delay=".6"><a href="#">Event <span>(24)</span></a></li>
                                <li data-animate="fadeInUp" data-delay=".7"><a href="#">Hosting <span>(236)</span></a></li>
                            </ul>--%>
                        </div>


                    </aside>
                </div>
                <div class="col-lg-8">
                    <div class="blog blog-left">
                        <div class="row">

                            <asp:Repeater ID="rptLatestBlog" runat="server">
                                <ItemTemplate>
                                    <div class="col-md-6" data-animate="fadeInUp" data-delay=".1">
                                        <div class="single-post">
                                            <a href="<%# Page.GetRouteUrl("Blog",new { title= ToTitle(Eval("Title").ToString()), id=Eval("BlogId") }) %>">
                                                <img src="<%# Page.ResolveUrl("~/Files/BlogImages/"+Eval("ImageUrl")) %>" alt="">
                                            </a>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div class="post-content">
                                                <p class="post-info">
                                                    Posted on 
                                                    <a href="javascript:;"><%# string.Format("{0:MMMM dd, yyyy}", Eval("PublishDate")) %></a> by 
                                                    <a href="javascript:;"><%# Eval("Author") %></a>
                                                </p>
                                                <h3>
                                                    <a href="javascript:;"><%# Eval("Title") %></a></h3>
                                                <a href="<%# Page.GetRouteUrl("Blog",new { title= ToTitle(Eval("Title").ToString()), id=Eval("BlogId") }) %>">Reading Continue<i class="fas fa-caret-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
