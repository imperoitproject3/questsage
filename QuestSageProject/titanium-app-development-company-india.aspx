﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NewMaster.master" AutoEventWireup="true" CodeBehind="titanium-app-development-company-india.aspx.cs" Inherits="QuestSageProject.titanium_app_development_company_india" %>

<%@ Register Src="~/Contact.ascx" TagPrefix="uc1" TagName="ContactUC" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <title>Titanium App Development Company India, Titanium Developers</title>
    <meta name="description" content=" QuestSage - We are leading Titanium app development company in India, our Titanium app developers develop top notch cross platform Titanium mobile applications for Android and iOS. " />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="title-bg-titanium">
        <div class="container">
            <div class="page-title text-white text-center">
                <h1 data-animate="fadeInUp" data-delay="1.2">Titanium App Development Company India</h1>
                <%--<h2 data-animate="fadeInUp" data-delay="1.4">Some Tagline here</h2>--%>
            </div>
        </div>
    </section>

    <section class="pt-70">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="single-post-page blog-left">
                        <div class="blog-details">
                            
                            <div class="post-detail-content">
                                <p data-animate="fadeInUp" data-delay=".2">
                                    In today’s times, mobile apps prove to be a game changer. It is easy to use and easy to reach billions of online users with just one touch. Mobile apps are easy to build whether it is Android or iOS. Both the platforms are immensely popular, and it is hard to ignore any one of them.<br />
                                    Any app development company can aim at targeting maximum user base with these platforms. For any mobile app development company, it is challenging to develop platform-specific apps. It requires a lot of time and resources to build separate apps working in the exact same way. The solution came with the invention of cross-platform technologies. When it comes to app development with cross-platform technologies, there is nothing better than Titanium. 


                                </p>




                                <ul class="list-group">
                                    <li class="list-group-item">We at QuestSage develop apps that seamlessly work on all the leading mobile platforms. 
                                    </li>
                                    <li class="list-group-item">As an adept Titanium app development company, our team of Titanium app developers are skilled with the knowledge of cross-platform app development with Titanium. 
                                    </li>
                                    <li class="list-group-item">We develop mobile apps for the main platforms - Android and iOS providing notable features that live up to mobile standards and user expectations. 
                                    </li>
                                    <li class="list-group-item">Our developers also develop smart and interactive mobile apps and continuously thriving for smarter and better Titanium app development
                                    </li>
                                    <li class="list-group-item">We can also realize ideas to reality, designing and developing apps that can do wonders for you and your end-users.

                                    </li>



                                </ul>
                                <br />
                                <div class="post-image">
                                    <img src="img/services/titanium.png" alt="" data-animate="fadeInUp" data-delay=".1">
                                    <%--       <a href="contact.aspx">Titanium app development
                                </a>--%>
                                </div>
                                <br />

                                <h3>Why QuestSage promotes the use of Titanium?</h3>

                                <ul class="list-group">
                                    <li class="list-group-item">Titanium is a popular mobile app development framework with a platform-independent API that aids the access to the native mobile functionality. 
                                    </li>
                                    <li class="list-group-item">Our experienced developers are well-versed with the use of modern web technologies and languages. 
                                    </li>
                                    <li class="list-group-item">They can develop cross-platform and responsive applications for all devices - mobile, desktop and tablets. 
                                    </li>
                                    <li class="list-group-item">They are best in creating rich cross-platform mobile apps from a single JavaScript-based SDK. We have a profound experience of native mobile functionality. Thus, as an emerging Titanium app development company in India & USA, we focus on delivering innovative applications. 
                                    </li>
                                </ul>
                                <br />
                                <p>
                                    Our Titanium app developers trust Titanium because it is a scalable solution that can work for all leading mobile OS. It helps you develop mobile apps that work on Android, iOS, Windows and so on. 
                                </p>



                                <hr />
                                <div class="comment-form">
                                    <h3 class="widget-title" data-animate="fadeInUp" data-delay=".1">Want to develop a smart and interactive mobile app?<br />
                                        Share us your business needs and requirements; we will provide you with the adequate solutions with our engaging customer-oriented mobile applications. 

                                    </h3>

                                    <uc1:ContactUC runat="server" ID="ContactUC" />

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="sidebar-widget" data-animate="fadeInUp" data-delay=".1">
                        <h3 class="widget-title" data-animate="fadeInUp" data-delay=".2">Client Speaks!

                        </h3>

                        <div class="why-us-video text-center text-white position-relative" data-animate="fadeInUp" data-delay=".1">

                            <img src="img/Video/bob.png" alt="">
                            <p>
                                <a class="pl-video" data-toggle="modal" data-target="#video-popup">
                                    <img src="img/play.png" alt="">
                                </a><span></span>
                            </p>


                        </div>
                    </div>
                    <br />
                    <br />

                    <aside>
                        <div class="sidebar-widget" data-animate="fadeInUp" data-delay=".1">
                            <h3 class="widget-title" data-animate="fadeInUp" data-delay=".2">Categories for Titanium Mobile Application Development

                            </h3>
                            <p>
                                Our talented team of Titanium app developers in India & USA has immensely contributed their skills to develop the rich native application for various platforms. They have learned how to power the platform with better quality apps for the global clients. We cater our app services for all the businesses and industries.
                            </p>

                            <ul class="sidebar-list list-unstyled ">


                                <li class="list-group-item"><b>Entertainment: </b>
                                    <br />
                                    We develop crazy and fun-oriented apps that drive you crazy. Our apps incorporate the features that give a joyous experience. We have developed apps for - videos, songs, chat applications and much more. 
                                </li>
                                <li class="list-group-item"><b>Utility:</b><br />
                                    We can help you develop a variety of the utility apps. These apps enhance your device's functionality, optimize the performances and make it more productive.
                                </li>
                                <li class="list-group-item"><b>Games:</b><br />
                                    We develop gaming applications with Titanium. With Titanium, we develop amazing and entertaining gaming applications that give the players a stupendous experience.
                                </li>
                                <li class="list-group-item"><b>e-Commerce:</b><br />
                                    Our e-Commerce applications will help you boost your customer base. You can help you generate and boost your business revenues. 
                                </li>
                                <li class="list-group-item"><b>Social Networking:</b><br />
                                    We offer engaging social networking apps that help you connect to your networks and build more effective networks. 
                                </li>
                                <li class="list-group-item"><b>Fashion and Lifestyle:</b><br />
                                    Our lifestyle applications help you to keep updated with the fashion, trends and the lifestyle.
                                </li>
                                <li class="list-group-item"><b>Finance and Business:</b><br />
                                    Our business application offers an individual to get updated with the finance, stocks, business strategies and revolutions in the daily world. 
                                </li>
                                <li class="list-group-item"><b>Healthcare:</b><br />
                                    “Stay fit, Stay Healthy” - a prime motto of our health applications. We develop applications that can track your fitness, daily activities and help you stay healthy. 
                                </li>
                            </ul>

                        </div>
                    </aside>
                </div>
            </div>
        </div>
        <div class="modal fade" id="video-popup" tabindex="-1" role="dialog" aria-labelledby="videoLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <p>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </p>
                        <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Wfj1W2htBIs?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

