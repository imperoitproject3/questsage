﻿using Newtonsoft.Json.Linq;
using QuestSageProject.DAL;
using QuestSageProject.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuestSageProject
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindBlogs();
            }

        }

        public static string ToTitle(string value)
        {
            return value.Replace(" ", "-").ToLower();
        }
        private void BindBlogs()
        {
            QuestsageEntities _ctx = new QuestsageEntities();
            List<BlogMasterModel> blogs = (from b in _ctx.BlogMaster
                                           select new BlogMasterModel
                                           {
                                               BlogId = b.BlogId,
                                               Title = b.Title,
                                               Tag = b.Tag,
                                               Author = b.Author,
                                               ImageUrl = b.ImageUrl,
                                               PublishDate = b.PublishDate,
                                               BlogBody = b.BlogBody
                                           }).OrderByDescending(x => x.PublishDate).Take(3).ToList();

            if (blogs != null && blogs.Any())
            {
                blogs.ForEach(x =>
                {
                    x.BlogBody = x.BlogBody.Length > 100 ? x.BlogBody.Substring(0, 100) : x.BlogBody;
                });
                rptBlogs.DataSource = blogs;
            }
            else
                rptBlogs.DataSource = null;
            rptBlogs.DataBind();
        }

        [WebMethod]
        public static bool IsReCaptchValid(string captchaResponse)
        {
            string secretKey = System.Web.Configuration.WebConfigurationManager.AppSettings.Get("SecretKey");
            var result = false;
            var apiUrl = "https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}";
            var requestUri = string.Format(apiUrl, secretKey, captchaResponse);
            var request = (HttpWebRequest)WebRequest.Create(requestUri);

            using (WebResponse response = request.GetResponse())
            {
                using (StreamReader stream = new StreamReader(response.GetResponseStream()))
                {
                    JObject jResponse = JObject.Parse(stream.ReadToEnd());
                    var isSuccess = jResponse.Value<bool>("success");
                    result = (isSuccess) ? true : false;
                }
            }
            return result;
        }
    }
}